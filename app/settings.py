import os
import typing as tp

ENVS = dict()
ENVVAR_TYPE = tp.Union[str, int, bool, None]


def parse_env_variable(var_name: str, as_int: bool = False,
                       as_bool: bool = False, default: ENVVAR_TYPE = None) -> ENVVAR_TYPE:
    global ENVS
    var: tp.Union[str, None] = os.getenv(var_name)
    parsed_var: ENVVAR_TYPE = None

    if var is None:
        if default is None:
            raise EnvironmentError(f'{var_name} is not defined!')

        return default

    if as_int or var_name.upper().endswith('_PORT'):
        parsed_var = int(var)
    elif as_bool or var_name.upper().startswith('IS_'):
        parsed_var = var.upper() == 'TRUE'
    else:
        parsed_var = var

    ENVS[var_name] = parsed_var
    return parsed_var

DEBUG = parse_env_variable('DEBUG', as_bool=True, default=False)
MONITORING = parse_env_variable('MONITORING', as_bool=True, default=True)
REDIS_DB = parse_env_variable('REDIS_DB', as_int=True, default=2)
USE_FILE_LOG = parse_env_variable('USE_FILE_LOG', as_bool=True, default=False)