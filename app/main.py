import json

from pipeline.queue import TaskQueue
from pipeline.meta import DescriptablePool
from pipeline.task import Task
from pipeline.stages.input_stage.CameraManager import CameraManager
from pipeline import Pipeline
from settings import DEBUG
from utils.LoggerManager import LoggerManager
main_logger = LoggerManager.get_logger('Main')

import time

if __name__ == '__main__':
    if DEBUG:
        main_logger.warning('You are working in DEBUG mode')

    with open('configs/cash_detections.json', 'r') as f:
        pipeline = DescriptablePool.from_json_dict(json.load(f))
    pipeline.initialize()
    pipeline.start()
    while True:
        time.sleep(100000)

