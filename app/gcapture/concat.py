import cv2
import numpy as np
from tqdm import tqdm
import sys
import signal

class InterruptionHandler:
    INTERRUPTED = False
    CALLBACKS = []
    _INITIALIZED = False

    @staticmethod
    def initialize():
        if not InterruptionHandler._INITIALIZED:
            InterruptionHandler._INITIALIZED = True
            signal.signal(signal.SIGINT, InterruptionHandler._signal_handler)

    @staticmethod
    def add_callback(callback):
        InterruptionHandler.initialize()
        InterruptionHandler.CALLBACKS.append(callback)

    @staticmethod
    def _signal_handler(sig, frame):
        InterruptionHandler.INTERRUPTED = True
        for callback in InterruptionHandler.CALLBACKS:
            callback()


class VideoSaver:

    def __init__(self, path: str, shape: tuple, fps: int, monitor_interruption=True,
                 fourcc=cv2.VideoWriter_fourcc(*'MP4V')):
        self.shape = shape
        self.video_writer = cv2.VideoWriter(path, fourcc, fps, self.shape)
        self.closed = False
        if monitor_interruption:
            InterruptionHandler.add_callback(self.release)

    def write(self, frame: np.ndarray):
        if frame is None or self.closed:
            return
        H, W = frame.shape[:2]
        if (W, H) != self.shape:
            frame = cv2.resize(frame, self.shape[:2])
        if frame.dtype != np.uint8:
            frame = np.uint8(frame / frame.max() * 255.)
        self.video_writer.write(frame)

    def release(self):
        if not self.closed:
            self.video_writer.release()
            self.closed = True


caps = [cv2.VideoCapture(f'{i}.avi') for i in range(4)]
cap0 = cv2.VideoCapture('0.avi')
H, W = cap0.read()[1].shape[:2]
ln = int(cap0.get(cv2.CAP_PROP_FRAME_COUNT))
vs = VideoSaver('concatted.avi', (W * 2, H * 2), 30)
cap0.release()
for _ in tqdm(range(ln)):
    frames = [cap.read()[1] for cap in caps]
    f0 = np.hstack(frames[:2])
    f1 = np.hstack(frames[2:])
    f = np.vstack([f0, f1])
    vs.write(f)
vs.release()
