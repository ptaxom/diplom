#!/bin/bash
mkdir -p build
cd build
rm master slave libpyGCapture.so
cmake -DBUILD_MASTER=ON-DH265=OFF -DWITH_OPENCV=ON -DBUILD_LIB=ON -DMESSAGE_VERBOSE_HIGH=ON .. && make -j 4
export GST_DEBUG=2
test -f "master" && test -f "slave" && ./master