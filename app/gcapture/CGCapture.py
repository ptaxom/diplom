from __future__ import absolute_import

import numpy as np
from ctypes import *
from tqdm import tqdm
import time 
import typing as tp

LOAD_LIBRARY = True
if LOAD_LIBRARY:
    # Constants area
    lib_path = 'build/libpyGCapture.so'
    slave_path = "build/slave"
    MEMKEY = 6000 - 3


    #Wrapper area
    lib = CDLL(lib_path, RTLD_GLOBAL)
    #IPC_COMMAND
    NO_ACTION, SHUTDOWN, RESTART, SLAVE_INITIATED = 0, 1, 2, 3
    #IPC_STATUS
    UNKNOWN, SUCCESS, FAILURE, IN_PROCESS, CLIENT_ERROR = 0, 1, 2, 3, 4

    create_captures = lib.create_captures
    create_captures.argtypes = [POINTER(c_char_p), c_int, c_int, c_int, c_int, c_char_p, c_int, POINTER(c_char_p)] #urls, n_urls, fps, width, height, #log files

    release_captures  = lib.release_captures

    get_pictures_lock_all = lib.get_pictures_lock_all
    get_pictures_lock_all.restype = POINTER(c_int8)
    
    is_running = lib.is_running
    is_running.restype = c_int

    get_event_count = lib.get_event_count
    get_event_count.restype = c_int

    set_command = lib.set_command
    set_command.argtypes = [c_int, c_int] #child_id, IPC_COMMAND
    set_command.restype = c_int

    execute_commands = lib.execute_commands
    execute_commands.restype = c_int

    free = lib.free

def strings_list_to_C_array(strings: tp.List[str]):
    """
        Converts python's list of string to C compatible nested arrays of chars
    """
    c_arr = (c_char_p * len(strings))()
    for i in range(len(strings)):
        c_arr[i] = strings[i].encode('utf-8')
    return c_arr

class CGCapture:
    
    def __init__(self, urls: tp.List[str], 
                        fps: int, width: int, height:int,
                        log_files: tp.List[str],
                        is_h264_stream: tp.List[int],
                        slave_fps=30):
        self.fps = fps
        self.width = width
        self.height = height
        self.urls = urls
        self.mem_size = width * height * 3
        self.delay = 1. / fps
        self.t0 = time.time()
        
        urls_arr = strings_list_to_C_array(urls)
        logs_arr = strings_list_to_C_array(log_files)
        encodings_array = (c_int * len(is_h264_stream))(*is_h264_stream)

        status = create_captures(urls_arr, len(urls), 
                                slave_fps, width, height,
                                slave_path.encode('utf-8'), 
                                MEMKEY, logs_arr,
                                encodings_array 
                                )
        
        
    def grab_frames(self):
        if not is_running():
            raise RuntimeError('One of streams is OFF!')
        images = []
        row_data = get_pictures_lock_all()
        
        for im_num in range(len(self.urls)):
            im = np.zeros((self.height, self.width, 3), dtype=np.uint8)
            arr = np.ascontiguousarray(im.flat, dtype=np.uint8)
            data = arr.ctypes.data_as(POINTER(c_byte))
            new_address = cast(addressof(row_data.contents) + self.mem_size * im_num, POINTER(c_char))
            memmove(data, new_address, self.mem_size)
            images.append(im)
        free(row_data)
        return images
            
    def release(self):
        release_captures()

    def read(self):
        while True:
            t1 = time.time()
            if t1 - self.t0 > self.delay:
                images = self.grab_frames()
                self.t0 = time.time()
                break
        return images