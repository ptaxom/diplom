#!/bin/bash
mkdir -p build
cd build
rm master slave libpyGCapture.so
cmake -DBUILD_MASTER=OFF -DBUILD_LIB=ON -DWITH_OPENCV=OFF -DCMAKE_BUILD_TYPE=Release .. && make -j 4