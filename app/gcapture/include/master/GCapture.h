#pragma once
#include <semaphore.h>
#include <shared/flags.h>
#include <shared/interaction.h>

typedef struct _LibraryState
{
    int total_captures;
    int fps;
    int width;
    int height;
    int mem_key_sem;
    int mem_sem_descriptor;
    sem_t* shared_semaphores;
    int mem_im_descriptor;
    void *shared_images;
    int *barrier_value;
    int inited;
    struct _LibraryState *shared_state;
    int state_mem_descriptor;

    int is_running;
    // IPC communication
    pid_t slave_pids[32]; // Max 32 clients
    SlaveConfig *shared_commands;
    int mem_command_descriptor;
} LibraryState;

int create_captures(const char *rtsp_urls[], const int urls_length, 
                    const int fps, const int width, const int height,
                    const char* slave_path, int shared_mem_key,
                    const char *log_files[], const int* is_h264_encoded);

int create_shared_state(int shared_state_memkey);

int attach_to_shared_state(int shared_state_memkey);

void release_captures();

char *get_pictures_lock_all();

int is_running();

void SIGUSR1_handler(int signum);

int get_event_count();

int set_command(int child_id, IPC_COMMAND command_value);

int get_status(int child_id);

int execute_commands();

extern LibraryState* state;