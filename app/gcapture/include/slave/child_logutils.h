#pragma once
#include <shared/logutils.h>
#include <slave/structures.h>



void child_log_message(ClientPayload *payload, LOG_LEVEL level, const char *message);

void console_child_log_message(ClientPayload *payload, LOG_LEVEL level, const char *message);