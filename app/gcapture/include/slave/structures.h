#pragma once
#include <gst/gst.h>
#include <semaphore.h>
#include <shared/interaction.h>
#include <stdio.h>


typedef struct _ClientPayload
{
    char *shared_space;
    GstSample* sample;
    size_t image_size;
    sem_t *semaphore;
    int child_id;
    SlaveConfig *config;
    FILE *log_file;
    GMainLoop *loop;
    GstElement *pipeline;
    GstElement *rtspsrc;
    char str_pipeline[2048];
} ClientPayload;