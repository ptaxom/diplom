#pragma once
#include <slave/gstutils.h>
#include <slave/child_logutils.h>
#include <shared/interaction.h>


void shutdown(ClientPayload *payload);

void restart(ClientPayload *payload);