#pragma once
#include <gst/gst.h>
#include <slave/structures.h>
#include <semaphore.h>
#include <shared/interaction.h>
#include <stdio.h>

typedef unsigned long long ull;

GstElement* find_element(GstPipeline* pipeline, const char* element_name);
ull get_time();

void* copy_image_async_trywait(void* data);

int state_changed_to(GstElement *pipeline, GstState expected_state, int timeout_seconds);

int start_pipeline(GstElement *pipeline);

int stop_pipeline(GstElement *pipeline);

void init_pipeline(ClientPayload *payload);

int cleanup_pipeline(ClientPayload *payload);

void configure_pipeline(ClientPayload *payload);

void attach_sample_callback(ClientPayload *payload);

GstFlowReturn new_sample (GstElement *sink, ClientPayload *payload);


void rtsp_errors_capture(GstDebugCategory *category,  GstDebugLevel level,
                    const gchar *file, const gchar *function,
                    gint line, GObject *object,
                    GstDebugMessage *message, gpointer data);