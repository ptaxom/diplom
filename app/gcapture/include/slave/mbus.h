#pragma once
#include <glib-unix.h>
#include <gst/gst.h>
#include <slave/gstutils.h>

gboolean my_bus_callback (GstBus * bus, GstMessage * message, gpointer data);

int configure_message_bus(GstElement* pipeline, ClientPayload *payload);

void concat_tags(const GstTagList *list, const gchar *tag, gpointer data);