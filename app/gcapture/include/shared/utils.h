#pragma once
#include <semaphore.h>
#include <sys/shm.h>
#include <stdio.h>

void print_semaphore_value(sem_t *semaphore, const char *sem_name);

void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result);

void *attach_to_memory(int mem_descriptor, size_t mem_size);

void update_barrier(sem_t *semaphore_mem, int total_captures);