#pragma once
#include <shared/flags.h>

 #ifdef __cplusplus
 #define EXTERNC extern "C"
 #else
 #define EXTERNC
 #endif

#ifdef OPENCV
    EXTERNC void show(char *);
    EXTERNC int show_multi_images(char *data, int total_captures, int width, int height);
    EXTERNC void* allocate_savers(int cnt, int fps, int width, int height);
    EXTERNC void save_frames(void *writers, char *data, int cnt, int width, int height);
    EXTERNC void deallocate_savers(void *writers);
#endif

#undef EXTERNC
