#pragma once
#include <stdio.h>

typedef enum _LOG_LEVEL{
    LOG_INFO,
    LOG_WARN,
    LOG_ERROR
} LOG_LEVEL;


void log_message(FILE* log_file, LOG_LEVEL level, const char* message);

void flog_message(FILE* log_file, LOG_LEVEL level, const char* message);

void clog_message(LOG_LEVEL level, const char* message);


char *get_current_time();