#pragma once


typedef enum _IPC_COMMAND{
    NO_ACTION = 0,          // Child will ignore this command
    SHUTDOWN = 1,           // Child will exit on this command
    RESTART = 2,            // Child will restart pipeline on this command
    SLAVE_INITIATED = 3     // Child requested master to check previous result of execution of previous command
} IPC_COMMAND;

typedef enum _IPC_STATUS{
    UNKNOWN = 0,            // Default status
    SUCCESS = 1,            // Previous command was executed succesefully
    FAILURE = 2,            // Previous command was failed
    IN_PROCESS = 3,         // Last command still in process of execution
    CLIENT_ERROR = 4        // Error on client side, notification for master
} IPC_STATUS;

typedef struct _SlaveConfig
{
    IPC_COMMAND command;
    IPC_STATUS status;
    pid_t master_pid;
    char is_h264;
    char url[1024];
    int width;
    int height;
    int fps;
    char data[1024];
    char last_error[2048];
} SlaveConfig;
