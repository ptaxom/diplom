#include <master/GCapture.h>
#include <shared/utils.h>

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stddef.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

LibraryState* state = NULL;
void initialize_zero_semaphore();
void print_zero_semaphore_value()
{
    print_semaphore_value((sem_t*)state->shared_semaphores, "IPC barrier");
}

#define CREATE_STR_VALUE(X) \
    char str_##X[10]; \
    sprintf(str_##X , "%d", state->X ) ; 

#define DEFAULT_SLAVE_PATH "./slave"
#define CHECK_STATUS(X) (state->shared_commands[i].status == X)
#define CHECK_COMMAND(X) (state->shared_commands[i].command == X)

int create_captures(const char *rtsp_urls[], const int urls_length, 
                    const int fps, const int width, const int height,
                    const char* slave_path, int shared_mem_key,
                    const char *log_files[], const int* is_h264_encoded)
{
    int semaphore_iterator = 0;
    pid_t pg_id = -1;
    int memkey_semaphores,
        memkey_images,
        memkey_commands;
    if (shared_mem_key < 0)
        memkey_semaphores = DEFAULT_MEMKEY_SEMAPHORES;
    else
        memkey_semaphores = shared_mem_key;
    memkey_images = memkey_semaphores * 2;
    memkey_commands = memkey_semaphores * 3;

    if (state == NULL)
    {
        if (signal(SIGUSR1, SIGUSR1_handler) == SIG_ERR)
        {
            fprintf(stderr, "Cannot create SIGUSR1 hand\n");
            goto malloc_error;   
        }
        state = malloc(sizeof(LibraryState));
        if (state == NULL)
        {
            fprintf(stderr, "Cannot allocate memory\n");
            goto malloc_error;
        }
        memset(state, 0, sizeof(LibraryState));
        // init fields
        state->total_captures = urls_length;
        state->width = width;
        state->height = height;
        state->fps = fps;
        state->mem_key_sem = memkey_semaphores;
        state->shared_state = NULL;
        state->mem_command_descriptor = shmget(memkey_commands, sizeof(SlaveConfig) * urls_length, IPC_CREAT | 0666);
        if (state->mem_command_descriptor < 0)
        {
            perror("Cannot create shared memory for commands");
            goto shmget_error_cmd;
        }
        // attaching to shared memory of commands
        state->shared_commands = shmat(state->mem_command_descriptor, NULL, 0);
        if (state->shared_commands < 0 || state->shared_commands == NULL)
        {
            perror("Cannot attach to shared memory for commands");
            goto shmat_error_cmd;
        }
        memset(state->shared_commands, 0, sizeof(SlaveConfig) * urls_length);

        size_t captures_size = width * height * 3 * urls_length * sizeof(u_int8_t),
               sem_size = (1 + urls_length) * sizeof(sem_t) + sizeof(int);
        // creating area of shared memory for semaphores
        state->mem_sem_descriptor = shmget(memkey_semaphores, sem_size, IPC_CREAT | 0666);
        if (state->mem_sem_descriptor < 0)
        {
            perror("Cannot create shared memory for semaphores");
            goto shmget_error_sem;
        }
        // attaching to shared memory of semaphores
        state->shared_semaphores = shmat(state->mem_sem_descriptor, NULL, 0);
        if (state->shared_semaphores < 0)
        {
            perror("Cannot attach to shared memory for semaphores");
            goto shmat_error_sem;
        }
        state->barrier_value = (int*)&state->shared_semaphores[urls_length + 1];
        // creating area of shared memory for images
        state->mem_im_descriptor = shmget(memkey_images, captures_size, IPC_CREAT | 0666);
        if (state->mem_im_descriptor < 0)
        {
            perror("Cannot create shared memory for images");
            goto shmget_error_im;
        }
        // attaching to shared memory of images
        state->shared_images = shmat(state->mem_im_descriptor, NULL, 0);
        if (state->shared_images < 0)
        {
            perror("Cannot attach to shared memory for images");
            goto shmat_error_im;
        }
        //
        // creating POSIX semaphores
        *state->barrier_value = urls_length;
        for(; semaphore_iterator < 1 + urls_length; ++semaphore_iterator)
        {
            // first semaphore is barrier synchronizer for all processes to compensate GSTREAMER ASYNC_STATE_CHANGE
            int semaphore_initial_value = 1;
            sem_t *semaphore = &state->shared_semaphores[semaphore_iterator];
            
            int status = sem_init(semaphore, IPC_SEMAPHORE, semaphore_initial_value);
            if (status < 0)
                {
                    fprintf(stderr, "Semaphore %d\n", semaphore_iterator);
                    perror("Cannot init semaphore");
                    goto sem_init_error;
                }
        }
        // launching tasks
        CREATE_STR_VALUE(total_captures)
        CREATE_STR_VALUE(mem_key_sem)

       pid_t master_pid = getpid();
        for(int i = 0; i < urls_length; ++i)
        {
            char str_child_id[10];
            sprintf(str_child_id, "%d", i);
            pid_t pid = fork();
            if (pid == 0)
            {
                char *log_file_name = calloc(256, sizeof(char));
                if (log_files == NULL || log_files[i] == NULL)
                    sprintf(log_file_name, "./cam_%d.log", i);
                else
                    strcpy(log_file_name, log_files[i]);
                // IPC interaction initialization
                state->shared_commands[i].status = UNKNOWN;
                state->shared_commands[i].command = NO_ACTION;
                state->shared_commands[i].master_pid = master_pid;
                // Configuration
                state->shared_commands[i].is_h264 = is_h264_encoded[i];
                state->shared_commands[i].width = width;
                state->shared_commands[i].height = height;
                state->shared_commands[i].fps = fps;
                strcpy(state->shared_commands[i].url, rtsp_urls[i]);


                int status = -1;
                if (slave_path)
                {
                    status = execl(slave_path, "", str_mem_key_sem,
                        str_total_captures, str_child_id,  log_file_name, NULL);
                }
                else
                {
                    status = execl(DEFAULT_SLAVE_PATH, "", str_mem_key_sem,
                        str_total_captures, str_child_id, log_file_name, NULL);
                }
                    
                if (status < 0)
                {
                    perror("Cannot exec");
                    exit(1);
                }
                exit(0);
            }           
            else if (pid > 0) 
            {
                state->slave_pids[i] = pid;
            }
        }
        // waiting 40s for initialization
        printf("Waiting child processes\n");
        sleep(5);
        pg_id = getpgrp();
        #ifndef NDEBUG
            for(int tries = 0; tries < urls_length * 4; tries++)
        #else
            for(int tries = 0; tries < urls_length * 2; tries++)
        #endif
        {
            struct timespec delay;
            clock_gettime(CLOCK_REALTIME, &delay);
            delay.tv_sec += 40;

            if (sem_timedwait(state->shared_semaphores, &delay) < 0)
            {
                perror("Cannot wait for child initialization");
                goto child_wait_error;
            }
            else
            {
                printf("%d cameras not inited.\n", *state->barrier_value);
                if (*state->barrier_value == 0)
                    break;
                sem_post(state->shared_semaphores);
                sleep(5);
            }
        }
        if (*state->barrier_value == 0)
            printf("All childs inited\n");
        else
            {
                fprintf(stderr, "Cannot init all childs.\n");
                goto child_wait_error;
            }

        state->is_running = 1;
        return 0;
    }
    else
    {
        return 1;       
    }

child_wait_error:
    if (pg_id > 0)
        killpg(pg_id, SIGKILL);
sem_init_error:
    semaphore_iterator -= 1;
    for(;semaphore_iterator >= 0; semaphore_iterator--)
    {
        sem_destroy((sem_t*)(state->shared_semaphores + sizeof(sem_t) * semaphore_iterator));
    }
    shmdt(state->shared_images);
shmat_error_im:
    shmctl(state->mem_im_descriptor, IPC_RMID, NULL);
shmget_error_im:
    shmdt(state->shared_semaphores);
shmat_error_sem:
    shmctl(state->mem_sem_descriptor, IPC_RMID, NULL);
shmget_error_sem:
    shmdt(state->shared_commands);
shmat_error_cmd:
    shmctl(state->mem_command_descriptor, IPC_RMID, NULL);
shmget_error_cmd:
    free(state);
malloc_error:
    return 1;
}


void release_captures()
{
    if (state)
    {
        for(int i = 0; i < state->total_captures; i++)
            state->shared_commands[i].command = SHUTDOWN;
        killpg(getpgrp(), SIGUSR1);
        for(int i = 0; i < state->total_captures + 1; i++)
        {
            sem_destroy((sem_t*)(state->shared_semaphores + sizeof(sem_t) * i));
        }
        shmdt(state->shared_semaphores);
        shmctl(state->mem_sem_descriptor, IPC_RMID, NULL);
        shmdt(state->shared_images);
        shmctl(state->mem_im_descriptor, IPC_RMID, NULL);
        shmdt(state->shared_commands);
        shmctl(state->mem_command_descriptor, IPC_RMID, NULL);
        free(state);
        printf("Released all.\n");
        state = NULL;
    }
}

char *get_pictures_lock_all()
{
    size_t pictures_size = state->total_captures * state->width * state->height * 3;
    char *pictures = malloc(pictures_size);
    sem_t* child_lockers = (sem_t*)(state->shared_semaphores + 1 * sizeof(sem_t));
    // for(int i = 0; i < state->total_captures; i++)
    //     sem_wait(&(child_lockers[i]));
    memcpy(pictures, state->shared_images, pictures_size);
    // for(int i = 0; i < state->total_captures; i++)
    //     sem_post(&(child_lockers[i]));
    return pictures;    
}


int create_shared_state(int shared_state_memkey)
{
    if (!state->shared_state)
    {
        // creating area of shared memory for placing there shared state, for attaching from other processors
        state->state_mem_descriptor = shmget(shared_state_memkey, sizeof(LibraryState), IPC_CREAT | 0666);
        if (state->state_mem_descriptor < 0)
        {
            perror("Cannot create shared memory for state");
            return 1;
        }
        state->shared_state = shmat(state->state_mem_descriptor, NULL, 0);
        if (state->shared_state < 0)
        {
            perror("Cannot attach to shared memory for state");
            goto shmat_error;
        }
        memcpy(state->shared_state, state, sizeof(LibraryState));
        return 0;
        shmat_error:
            shmctl(state->state_mem_descriptor, IPC_RMID, NULL);
        return 1;
    }
    else
        return 2;    
}


int attach_to_shared_state(int shared_state_memkey)
{
    if (!state)
    {
        // attaching to shared state
        int state_mem_descriptor = shmget(shared_state_memkey, sizeof(LibraryState), IPC_CREAT | 0666);
        if (state_mem_descriptor < 0)
        {
            perror("Cannot create shared memory for state");
            return 1;
        }
        LibraryState *shared_state = shmat(state_mem_descriptor, NULL, 0);
        if (shared_state < 0)
        {
            perror("Cannot attach to shared memory for state");
            goto shmat_error;
        }
        // initializing of own state from shared state
        state = malloc(sizeof(LibraryState));
        memcpy(state, shared_state, sizeof(LibraryState));

        state->state_mem_descriptor = state_mem_descriptor;
        state->shared_state = shared_state;
        // replacing descriptor and pointer from creator of state to own
        size_t captures_size = state->total_captures * state->height * state->width * 3 * sizeof(u_int8_t),
                sem_size = (1 + state->total_captures) * sizeof(sem_t) + sizeof(int);

        // creating area of shared memory for semaphores
        state->mem_sem_descriptor = shmget(state->mem_key_sem, sem_size, IPC_CREAT | 0666);
        if (state->mem_sem_descriptor < 0)
        {
            perror("Cannot create shared memory for semaphores");
            goto shmget_error_sem;
        }
        // attaching to shared memory of semaphores
        state->shared_semaphores = shmat(state->mem_sem_descriptor, NULL, 0);
        if (state->shared_semaphores < 0)
        {
            perror("Cannot attach to shared memory for semaphores");
            goto shmat_error_sem;
        }

        state->mem_im_descriptor = shmget(state->mem_key_sem * 2, captures_size, IPC_CREAT | 0666);
        if (state->mem_im_descriptor < 0)
        {
            perror("Cannot create shared memory for images");
            goto shmget_error_im;
        }
        // attaching to shared memory of images
        state->shared_images = shmat(state->mem_im_descriptor, NULL, 0);
        if (state->shared_images < 0)
        {
            perror("Cannot attach to shared memory for images");
            goto shmat_error_im;
        }

        state->mem_command_descriptor = shmget(state->mem_key_sem * 3, state->total_captures * sizeof(SlaveConfig), IPC_CREAT | 0666);
        if (state->mem_command_descriptor < 0)
        {
            perror("Cannot create shared memory for commands");
            goto shmget_error_cmd;
        }
        // attaching to shared memory of images
        state->shared_commands = shmat(state->mem_command_descriptor, NULL, 0);
        if (state->shared_commands < 0)
        {
            perror("Cannot attach to shared memory for commands");
            goto shmat_error_cmd;
        }

        return 0;

shmat_error_cmd:
    shmctl(state->mem_command_descriptor, IPC_RMID, NULL);
shmget_error_cmd:
    shmdt(state->shared_images);
shmat_error_im:
    shmctl(state->mem_im_descriptor, IPC_RMID, NULL);
shmget_error_im:
    shmdt(state->shared_semaphores);
shmat_error_sem:
    shmctl(state->mem_sem_descriptor, IPC_RMID, NULL);
shmget_error_sem:
    free(state);
shmat_error:
    shmctl(state_mem_descriptor, IPC_RMID, NULL);
    }
    else
        return 2;
}


int is_running()
{
    return state && state->is_running;
}

void SIGUSR1_handler(int signum)
// Handling that one of childs request
{
    if (signum == SIGUSR1 && state)
    {
        state->is_running = 0;
    }
}


int get_event_count()
{
    if (state)
    {
        int events = 0;
        for(int i = 0; i < state->total_captures; i++)
        {
            if (CHECK_STATUS(SUCCESS) || CHECK_STATUS(FAILURE))
                events++;
        }
        return events;
    }
    else
        return -1;
}

int set_command(int child_id, IPC_COMMAND command_value)
{
    if (state && child_id < state->total_captures)
    {
        if (state->shared_commands[child_id].status == IN_PROCESS)
            return 1;
        state->shared_commands[child_id].command = command_value;
        return 0;
    }
    return -1;
}

int get_status(int child_id)
{
    if (state && child_id < state->total_captures)
    {
        return state->shared_commands[child_id].status;
    }
    return -1;
}

int execute_commands()
{
    if (state)
    {
        int executed = 0;
        for(int i = 0; i < state->total_captures; i++)
            if ((state->shared_commands[i].status != IN_PROCESS) && (CHECK_COMMAND(SHUTDOWN) || CHECK_COMMAND(RESTART)))
            {
                kill(state->slave_pids[i], SIGUSR1);
                executed++;
            }
        return executed;
        
    }
    return -1;
}