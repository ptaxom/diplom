#include <master/GCapture.h>
#include <shared/vis.h>
#include <shared/utils.h>
#include <shared/flags.h>
#include <time.h>

const char *urls[] = {
    // "rtsp://10.30.30.196:8555/test",
    // "rtsp://10.42.0.100:8554/test",
    "rtsp://127.0.0.1:8554/test",
    "rtsp://127.0.0.1:8556/test",
    // "rtsp://127.0.0.1:8554/test",
    // "rtsp://127.0.0.1:8556/test",
};
const char is_h264_encoded[] = {1, 1, 1, 1};

int main(int argc, char *argv[])
{
    int total_captures = 2,
        fps = 30,
        width = 600,
        height = 400;
    long long nsec_delay = 1000 * 1000 * 1000  / (fps * 0.97);

    int status = create_captures(urls, total_captures, fps, width, height, NULL, -1, NULL, is_h264_encoded);  
    if (status == 0)
    {
        #if defined(OPENCV)
            #ifdef RECORD
                void *savers = allocate_savers(total_captures, fps, width, height);
                int fr_num = 0;
                struct timespec last_updated, now, diff;
                clock_gettime(CLOCK_REALTIME, &last_updated);
                long diffs = 0, n = 0;
                while (fr_num < 1000)
                {
                    clock_gettime(CLOCK_REALTIME, &now);
                    timespec_diff(&last_updated, &now, &diff);
                    if (diff.tv_sec > 0 || diff.tv_nsec > nsec_delay)
                    {
                        last_updated = now;
                        #ifdef TIME_MEASURE
                            diffs += (diff.tv_sec % 10) * 1000 + (diff.tv_nsec / 1000000);
                            n += 1;
                        #endif
                        char *pictures = get_pictures_lock_all(); 
                        save_frames(savers,pictures, total_captures, width, height);
                        fr_num++;
                    }
                }
                #ifdef TIME_MEASURE
                    float avg_time = (float)diffs / n;
                    printf("Master average call time: %fms\n ", avg_time);
                #endif
                deallocate_savers(savers);
            #else
                while(is_running())
                {
                    char *pictures = get_pictures_lock_all();
                    int key = show_multi_images(pictures, total_captures, width, height);
                    free(pictures);
                    if (key == 27)
                        break;
                    if (key == 114) // 'r'
                    {
                        printf("Reloading first client...");
                        set_command(1, RESTART);
                        int n_executed = execute_commands();
                        printf("Sended signal to %d clients...\n", n_executed);
                    }
                }
            #endif
        #else
            printf("Add -D WITH_OPENCV flag to cmake build\n");
        #endif
        release_captures();
    }
    else
        printf("Cannot create captures.");
    
    return status;
}