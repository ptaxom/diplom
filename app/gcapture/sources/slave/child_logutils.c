#include <slave/child_logutils.h>

void child_log_message(ClientPayload *payload, LOG_LEVEL level, const char *message)
{
    flog_message(payload->log_file, level, message);
    console_child_log_message(payload, level, message);
}

void console_child_log_message(ClientPayload *payload, LOG_LEVEL level, const char *message)
{
    char child_message[4096];
    sprintf(child_message, "CHILD [%d] %s", payload->child_id, message);
    clog_message(level, child_message);
}