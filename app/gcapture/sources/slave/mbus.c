#include <slave/mbus.h>
#include <shared/logutils.h>
#include <slave/child_logutils.h>

gboolean my_bus_callback (GstBus * bus, GstMessage * message, gpointer data)
{
    char log_string[2048] = {0};
    sprintf(log_string, "Got %s message", (char *)GST_MESSAGE_TYPE_NAME (message));
    ClientPayload *payload = data;
    FILE* log_file = payload->log_file;
    flog_message(log_file, LOG_INFO, log_string);


    switch (GST_MESSAGE_TYPE (message)) {
        case GST_MESSAGE_ERROR:{
            int raise_error = 1;
            GError *err;
            gchar *debug;
            
            gst_message_parse_error (message, &err, &debug);
            if (message->src)
                sprintf(log_string, "ERROR from element %s:", GST_OBJECT_NAME (message->src));
            else if (message->src && err->message)
                sprintf(log_string, "ERROR from element %s: %s", GST_OBJECT_NAME (message->src), err->message);
            else
                {
                    sprintf(log_string, "Unknown error with NULL params. Check gstreamer log instead of this.");   
                    raise_error = 0;
                }
            child_log_message(payload, LOG_ERROR, log_string);

            if (raise_error)
                (payload->config)->status = CLIENT_ERROR;

            g_error_free (err);
            g_free (debug);

            break;
        }
        case GST_MESSAGE_EOS: {
            (payload->config)->status = CLIENT_ERROR;
            child_log_message(payload, LOG_ERROR, "Stream ended");
            break;
        }
        case GST_MESSAGE_TAG: {
            GstTagList *tags = NULL;
            char *tags_str_list = calloc(4096, 1);
            gst_message_parse_tag (message, &tags);
            gst_tag_list_foreach(tags, concat_tags, tags_str_list);
            sprintf(log_string, "Got tags(%s) from element %s", tags_str_list + 2, GST_OBJECT_NAME (message->src));
            free(tags_str_list);            

            gst_tag_list_unref (tags);
            flog_message(log_file, LOG_INFO, log_string);
            break;  
        }
        #ifdef MESSAGE_VERBOSE_HIGH
            case GST_MESSAGE_STATE_CHANGED: {
                GstState old_state, new_state;

                gst_message_parse_state_changed (message, &old_state, &new_state, NULL);
                sprintf(log_string, "Element %s changed state from %s to %s.",
                        GST_OBJECT_NAME (message->src),
                        gst_element_state_get_name (old_state),
                        gst_element_state_get_name (new_state));
                flog_message(log_file, LOG_INFO, log_string);
                break;
            }
            case GST_MESSAGE_PROGRESS:{
                    flog_message(log_file, LOG_INFO, "Got progress message");
                break;
            }
            case GST_MESSAGE_STREAM_STATUS: {
                    GstStreamStatusType stream_status;
                    GstElement *owner;
                    gst_message_parse_stream_status(message, &stream_status, &owner);

                    flog_message(log_file, LOG_INFO, "Got stream status message");
                break;
            }
        #endif
        default: {
            #ifdef MESSAGE_VERBOSE_HIGH
                if (GST_MESSAGE_TYPE(message) != GST_MESSAGE_TAG)
                {
                    sprintf(log_string, "Got %s message", (char *)GST_MESSAGE_TYPE_NAME (message));
                    flog_message(log_file, LOG_INFO, log_string);
                }
            #endif
            break;
        }
    }

    return TRUE;
}


int configure_message_bus(GstElement* pipeline, ClientPayload *payload)
{
    GstBus *bus = NULL;
    bus = gst_pipeline_get_bus(pipeline);
    if (bus)
    {
        guint bus_watch_id = gst_bus_add_watch (bus, my_bus_callback, payload);
        gst_object_unref(bus);
    }
    else
        return -1;
}

void concat_tags(const GstTagList *list, const gchar *tag, gpointer data)
{
    char *description_line = (char*)data;
    sprintf(description_line, "%s, %s", description_line, tag);
}