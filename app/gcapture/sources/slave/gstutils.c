#include <slave/gstutils.h>
#include <slave/mbus.h>
#include <slave/child_logutils.h>
#include <shared/vis.h>
#include <shared/utils.h>
#include <shared/flags.h>

#include <string.h>
#include <stdio.h>
#include <pthread.h>


#define H264_PIPELINE "rtspsrc location=%s ! decodebin ! videoconvert ! video/x-raw,format=(string)BGR ! videoconvert ! videoscale ! video/x-raw,width=%d,height=%d ! videorate ! video/x-raw,framerate=%d/1 ! appsink emit-signals=true sync=false max-buffers=1 drop=true"
#define H265_PIPELINE "rtspsrc location=%s ! rtph265depay ! decodebin ! videoconvert ! video/x-raw,format=(string)BGR ! videoconvert ! videoscale ! video/x-raw,width=%d,height=%d ! videorate ! video/x-raw,framerate=%d/1 ! appsink emit-signals=true sync=false max-buffers=1 drop=true"

GstElement* find_element(GstPipeline* pipeline, const char* element_name)
{
  gint num_children = pipeline->bin.numchildren;
  GList *children = pipeline->bin.children;
  GstElement *element;
  gboolean finded_appsink = FALSE;
  for(int i = 0; i < pipeline->bin.numchildren && finded_appsink == FALSE; ++i)
  {
    element = (GstElement*)children->data;
    gchar *name = gst_object_get_name((GstObject*)element);
    // WTF, g_strv_contains segfault
    finded_appsink = (strstr(name, element_name) != NULL);
    g_free(name);
    children = children->next;
  }
  if (!finded_appsink)
    element = NULL;
  return element;
}


ull get_time()
{
    struct timeval tv;

    gettimeofday(&tv, NULL);

    ull milliseconds_since_epoch =
      (unsigned long long)(tv.tv_sec) * 1000 +
      (unsigned long long)(tv.tv_usec) / 1000;
    return milliseconds_since_epoch;
}

int cleanup_pipeline(ClientPayload *payload)
{
    if (payload && payload->pipeline)
    {
        stop_pipeline(payload->pipeline);
        gst_object_unref (payload->pipeline);
        payload->pipeline = NULL;
    }
    return 1;
}

int state_changed_to(GstElement *pipeline, GstState expected_state, int timeout_seconds)
{
    ull waiting_begin = get_time();
    // wait 30 seconds for initialization
    GstState current_state, pending_state;
    GstClockTime wait_time = (GstClockTime)(300 * 1000 * 1000); // 300ms for waiting changes
    while (get_time() - waiting_begin < timeout_seconds * 1000)
    {
        gst_element_get_state(pipeline, &current_state, &pending_state, wait_time);
        if (current_state == expected_state)
            return 1;
        g_usleep(300 * 1000); // 300ms for delay
    }
    return 0;
}

int start_pipeline(GstElement *pipeline)
{
    gst_element_set_state(pipeline, GST_STATE_PLAYING);
    return state_changed_to(pipeline, GST_STATE_PLAYING, 30);
}

int stop_pipeline(GstElement *pipeline)
{
    gst_element_set_state(pipeline, GST_STATE_NULL);
    return state_changed_to(pipeline, GST_STATE_NULL, 30);
}

void configure_pipeline(ClientPayload *payload)
// Create string representation of current pipeline
{
    SlaveConfig *config = payload->config;
    int width = config->width,
        height = config->height,
        fps = config->fps;
    #ifndef NDEBUG
        char debug_message[2048] = {0};
    #endif
    if (config->is_h264)
        sprintf(payload->str_pipeline, H264_PIPELINE, config->url, width, height, fps);
    else
        sprintf(payload->str_pipeline, H265_PIPELINE, config->url, width, height, fps);
    #ifndef NDEBUG
        sprintf(debug_message, "H.26%c pipeline for %s is \033[94m%s\033[0m", 
                '5' - config->is_h264,
                config->url,
                payload->str_pipeline
                );
        child_log_message(payload, LOG_INFO, debug_message);
    #endif
    
}

void init_pipeline(ClientPayload *payload)
{
    GstElement* pipeline;
    configure_pipeline(payload);
    payload->pipeline = gst_parse_launch(payload->str_pipeline, NULL);
    configure_message_bus(payload->pipeline, payload);
    int started = start_pipeline(payload->pipeline);
    if (!started)
    {
        child_log_message(payload, LOG_ERROR, "Cannot start pipeline");
        cleanup_pipeline(payload);
        return;
    }
    child_log_message(payload, LOG_INFO, "Changed pipeline state to PLAY");
    payload->rtspsrc = find_element(payload->pipeline, "rtspsrc");
}

void attach_sample_callback(ClientPayload *payload)
{
    GstElement *appsink = find_element(payload->pipeline, "appsink");
    #ifdef TIME_MEASURE
        clock_gettime(CLOCK_REALTIME, &start);
    #endif
    g_signal_connect (appsink, "new-sample", G_CALLBACK (new_sample), payload);
}


GstFlowReturn new_sample (GstElement *sink, ClientPayload *payload) 
// Callback to catch new frames from pipeline
{
  GstSample *sample; 
  
  g_signal_emit_by_name (sink, "pull-sample", &sample);
  if (sample) {
    int can_lock = sem_trywait(payload->semaphore);
    if (can_lock >= 0)
    {
    #ifdef TIME_MEASURE
        make_measure();
    #endif
      GstBuffer* buffer = gst_sample_get_buffer(sample);
      char* data;
      gsize size;
      gst_buffer_extract_dup(buffer, 0, payload->image_size, &data, &size);
      memcpy(payload->shared_space, data, payload->image_size);
      g_free(data);
      sem_post(payload->semaphore);
    }
    gst_sample_unref(sample);
    return GST_FLOW_OK;
  }
  return GST_FLOW_ERROR;
}


void rtsp_errors_capture(GstDebugCategory *category,  GstDebugLevel level,
                    const gchar *file, const gchar *function,
                    gint line, GObject *object,
                    GstDebugMessage *message, gpointer data)
// capture messages from error/debug stream and redirect them to message bus
// for example we have warning "No route to host" in stderr, but main pipeline will still trying to connect 
{
    ClientPayload *payload = data;
    // If get error from rtspsrc
    const char *char_message = gst_debug_message_get(message);
    if (payload && (payload->rtspsrc == object) && (level == GST_LEVEL_ERROR || level == GST_LEVEL_WARNING) && char_message)
    {
        #ifndef NDEBUG
            printf("CAPTURED: %s\n", char_message);
        #endif
        GstBus *bus = gst_pipeline_get_bus(payload->pipeline);
        if (bus)
        {
            GError *error = g_error_new(gst_resource_error_quark(), 228, "%s", char_message);
            GstMessage *message = gst_message_new_error(object, error, NULL);
            int posted = gst_bus_post(bus, message);
            g_error_free (error);
            gst_object_unref(bus);
        }
    }
}
