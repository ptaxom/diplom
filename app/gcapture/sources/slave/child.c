#include <shared/utils.h>
#include <slave/gstutils.h>
#include <shared/flags.h>
#include <shared/logutils.h>
#include <shared/interaction.h>
#include <slave/ipc_actions.h>

#include <glib-unix.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <semaphore.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <gst/gst.h>
#include <glib.h>
#include <signal.h>

static struct timespec start, end, diff;
long diffs = 0, n = 0;

void make_measure();

gboolean SIGUSR1_handler(gpointer data)
{
    ClientPayload *payload = (ClientPayload*)data;
    #ifndef NDEBUG
        child_log_message(payload, LOG_WARN, "Recieved master signal. Processing.");
    #endif
    char log_string[2048] = {0};
    SlaveConfig *config = payload->config;
    IPC_COMMAND command = config->command;
    if (command != NO_ACTION)
    {
        (payload->config)->status = IN_PROCESS;
        if (command == SHUTDOWN)
            shutdown(payload);
        if (command == RESTART)
            restart(payload);
        if (command != SLAVE_INITIATED)
            (payload->config)->command = NO_ACTION;
    }
    #ifdef TIME_MEASURE
        float avg_time = (float)diffs / n;
        printf("Average call time: %f\n", avg_time);
    #endif
    return G_SOURCE_CONTINUE;
}

int main(int argc, char *argv[])
{
    if (argc != 5)
    {
        fprintf(stderr, "Usage [sem_mem_key] [total_captures] [child_id] [logging_file]");
        exit(1);
    }
    int memkey = atoi(argv[1]);
    int total_captures = atoi(argv[2]);
    int child_id = atoi(argv[3]);

    ClientPayload *payload = malloc(sizeof(ClientPayload));
    payload->child_id = child_id;
    FILE *log_file;
    #ifndef NDEBUG
        log_file = fopen(argv[4], "w");
    #else
        log_file = fopen(argv[4], "w+");
    #endif
    if (log_file == NULL)
    {
        perror("Cannot open logfile\n");
        exit(1);
    }
    payload->log_file = log_file;
    
    payload->config = attach_to_memory(memkey * 3, sizeof(SlaveConfig) * total_captures);
    payload->config += child_id;
    SlaveConfig *config = payload->config;
    printf("CHILD[%d][pid: %d] with url %s\n", child_id, getpid(), config->url);

    payload->image_size = config->width * config->height * 3;
    payload->sample = NULL;
    payload->pipeline = NULL;
    size_t captures_size = config->width * config->height * 3 * total_captures * sizeof(u_int8_t),
               sem_size = (1 + total_captures) * sizeof(sem_t);

    sem_t *semaphore_mem = attach_to_memory(memkey, sem_size);
    void *image_mem = attach_to_memory(memkey * 2, captures_size);
    payload->semaphore = &semaphore_mem[child_id+1];
    payload->shared_space = image_mem + child_id * payload->image_size;
    

    int ret = 0;
    
    
    gst_init (&argc, &argv);
    gst_debug_add_log_function(rtsp_errors_capture, payload, NULL);
    payload->loop = g_main_loop_new (NULL, FALSE);
    init_pipeline(payload);
    g_unix_signal_add(SIGUSR1, SIGUSR1_handler, payload);
    if (payload->pipeline)
    {   
        update_barrier(semaphore_mem, total_captures);
        attach_sample_callback(payload);
        flog_message(log_file, LOG_INFO, "========= STARTED =========");
        g_main_loop_run (payload->loop);
        sleep(2);
        
        #ifdef TIME_MEASURE
            float avg_time = (float)diffs / n;
            printf("Average call time: %fms\n ", avg_time);
        #endif
    }
    else
    {
        free(payload);
    }
    return 0;
}

void make_measure()
{
  clock_gettime(CLOCK_REALTIME, &end);
  timespec_diff(&start, &end, &diff);
  diffs += (diff.tv_sec % 10) * 1000 + (diff.tv_nsec / 1000000);
  n += 1;
  start = end;
}