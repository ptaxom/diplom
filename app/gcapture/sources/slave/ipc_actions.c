#include <slave/ipc_actions.h>


void shutdown(ClientPayload *payload)
{
    if (payload)
    {
        cleanup_pipeline(payload->pipeline);
        (payload->config)->status = SUCCESS;
        child_log_message(payload, LOG_INFO, "CAMERA STOPPED by master");
        g_main_loop_quit(payload->loop);
        free(payload);
        payload = NULL;
    }
}

void restart(ClientPayload *payload)
{
    if (payload)
    {
        child_log_message(payload, LOG_WARN, "RESTARTING PIPELINE");
        int result = cleanup_pipeline(payload);
        if (result)
        {
            child_log_message(payload, LOG_INFO, "STOPPED PIPELINE");
            init_pipeline(payload);
            if (payload->pipeline)
            {
                attach_sample_callback(payload);
                child_log_message(payload, LOG_INFO, "RESTARTED PIPELINE");
                (payload->config)->status = SUCCESS;
                return;
            }
        }
        
            
    restart_failure:
        child_log_message(payload, LOG_ERROR, "CANNOT RESTART PIPELINE");
        (payload->config)->status = FAILURE;
    }
}