#include <shared/vis.h>
#include <shared/flags.h>
#ifdef OPENCV
    #include <opencv4/opencv2/opencv.hpp>
    #include <iostream>
    #include <string>

    void show(char *data)
    {
        cv::Mat image(416, 416, CV_8UC3, data);
        cv::imshow("Debug window", image);
        cv::waitKey(1);
    }

    int show_multi_images(char *data, int total_captures, int width, int height)
    {
        size_t image_size = width * height * 3;
        for(int i = 0; i < total_captures; i++)
        {
            // std::cout << "Showing images" << std::endl;
            cv::Mat image(height, width, CV_8UC3, data + image_size * i);
            std::string name = std::to_string(i);
            cv::imshow(name, image);
        }
        return cv::waitKey(10);
    }


    void* allocate_savers(int cnt, int fps, int width, int height)
    {
        std::vector<cv::VideoWriter> *vec = new std::vector<cv::VideoWriter>();
        for(int i = 0; i < cnt; i++)
        {
            std::string name = std::to_string(i) + ".avi";
            cv::VideoWriter writer(name, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, cv::Size(width, height));
            vec->push_back(writer);
        }
        return (void*)vec;
    }


    void save_frames(void *writers, char *data, int cnt,  int width, int height)
    {
        std::vector<cv::VideoWriter> *vec = static_cast<std::vector<cv::VideoWriter> *>(writers);
        size_t image_size = width * height * 3;
        for(int i = 0; i < cnt; i++)
        {
            cv::Mat image(height, width, CV_8UC3, data + image_size * i);
            std::string name = std::to_string(i);
            (*vec)[i].write(image);
        }
    }

    void deallocate_savers(void *writers)
    {
        std::vector<cv::VideoWriter> *vec = static_cast<std::vector<cv::VideoWriter> *>(writers);
        for(int i = 0; i < vec->size(); i++)
            (*vec)[i].release();
        delete vec;
    }

#endif