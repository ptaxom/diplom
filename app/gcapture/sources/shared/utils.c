#include <shared/utils.h>
#include <sys/sem.h>
#include <assert.h>
#include <stdio.h>

void print_semaphore_value(sem_t *semaphore, const char *sem_name)
{
    int value;
    sem_getvalue(semaphore, &value);
    printf("Semaphore %s value: %d\n", sem_name, value);
}

void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result)
{
    if ((stop->tv_nsec - start->tv_nsec) < 0) {
        result->tv_sec = stop->tv_sec - start->tv_sec - 1;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
    } else {
        result->tv_sec = stop->tv_sec - start->tv_sec;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec;
    }

    return;
}

void *attach_to_memory(int mem_descriptor, size_t mem_size)
{
    mem_descriptor = shmget(mem_descriptor, mem_size, 0666);
    if (mem_descriptor < 0)
    {
        perror("Cannot get shared memory\n");
        exit(1);
    }
    void *shared_memory = shmat(mem_descriptor, NULL, 0);
    if (shared_memory < 0)
    {
        perror("Cannot attach to memory\n");
        exit(1);
    }
    return shared_memory;
}


void update_barrier(sem_t *semaphore_mem, int total_captures)
{
    int status = sem_wait(semaphore_mem);
    int *barrier_value = (int*)&semaphore_mem[total_captures + 1];
    *barrier_value -= 1;
    status = sem_post(semaphore_mem);

    if (status < 0)
    {
        perror("Cannot post semaphore");
        exit(1);
    }
}