#include <shared/logutils.h>
#include <time.h>
#include <stdio.h>

void log_message(FILE* log_file, LOG_LEVEL level, const char* message)
{
    clog_message(level, message);
    flog_message(log_file, level, message);
}

void clog_message(LOG_LEVEL level, const char* message)
{
    char *timestamp = get_current_time();
    switch (level)
    {
        case LOG_INFO:
            printf("\033[92m[INFO]\033[0m %s: %s\n", timestamp, message);   
            break;
        case LOG_WARN:
            printf("\033[93m[WARN]\033[0m %s: %s\n", timestamp, message);   
            break;
        case LOG_ERROR:
            printf("\033[91m[ERROR]\033[0m %s %s\n", timestamp, message);   
            break;
    default:
        break;
    }
    free(timestamp);
}

void flog_message(FILE* log_file, LOG_LEVEL level, const char* message)
{
    char *timestamp = get_current_time();
    switch (level)
    {
    case LOG_INFO:
        fprintf(log_file, "[INFO] %s: %s\n", timestamp, message);
        break;
    case LOG_WARN:
        fprintf(log_file, "[WARN] %s: %s\n", timestamp, message);
        break;
    case LOG_ERROR:
        fprintf(log_file, "[ERROR] %s: %s\n", timestamp, message);
        break;
    default:
        break;
    }
    free(timestamp);
}


char *get_current_time()
{
    time_t timer;
    char* buffer = calloc(32, 1);
    struct tm* tm_info;

    timer = time(NULL);
    tm_info = localtime(&timer);

    strftime(buffer, 32, "%Y-%m-%d %H:%M:%S", tm_info);
    return buffer;
}