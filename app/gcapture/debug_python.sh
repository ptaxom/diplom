#!/bin/bash
mkdir -p build
cd build
rm master slave libpyGCapture.so
cmake -DBUILD_MASTER=ON-DH265=OFF -DBUILD_LIB=ON -DCMAKE_BUILD_TYPE=Release .. && make -j 4
export GST_DEBUG=2
test -f "libpyGCapture.so" && test -f "slave" && cd .. && python3 demo.py