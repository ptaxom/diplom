from __future__ import absolute_import

from threading import Lock, Condition
from datetime import datetime, timedelta
import typing as tp

from pipeline.task import Task
from pipeline.meta import DescriptablePool

from settings import DEBUG
from utils.monitoring import log_queue_status

class TaskQueue(DescriptablePool):

    # TODO: UML variable in methods section

    def __init__(self, object_name, acceptable_tags=None, max_batch_size=1, group_timeout=None, max_queue_size=50):
        super(TaskQueue, self).__init__(object_name)
        self.acceptable_tags: tp.Dict[str, tp.List[str]] = acceptable_tags
        self.max_batch_size: int = max_batch_size
        self.group_timeout: float =  group_timeout
        self.max_queue_size = max_queue_size
        if DEBUG:
            self.group_timeout = None

        self.queue_mutex = Lock()

        self.grouping_variable = Condition(self.queue_mutex)
        self.queue: tp.List[Task] = []

    def filter_queue(self):
        return list(filter(lambda task: task.invalidation_timeout > datetime.now(), self.queue))

    def push_data(self, task: Task):
        with self.grouping_variable:
            # wait for valid queue size
            self.grouping_variable.wait_for(lambda: len(self.queue) < self.max_queue_size)
            # accept all tags or if can consume
            if self.acceptable_tags is None or \
                any([value in self.acceptable_tags.get(tag, []) for tag, value in task.tags.items()]):
                self.queue.append(task)
                self.queue = self.filter_queue()
                log_queue_status(self)
                self.grouping_variable.notify_all()
    
    def pop_data(self):
        with self.grouping_variable:
            batch_barrier = lambda: len(self.filter_queue()) >= self.max_batch_size
            self.grouping_variable.wait_for(batch_barrier, timeout=self.group_timeout)
            self.queue = self.filter_queue()
            self.queue = sorted(self.queue, key=lambda x: x.order_id)

            consumed = self.queue[:self.max_batch_size]
            self.queue = self.queue[self.max_batch_size:]
            self.grouping_variable.notify_all()
            log_queue_status(self)
            return consumed

    @staticmethod
    def _from_json_dict(json: dict):
        return TaskQueue(**json)