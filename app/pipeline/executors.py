from __future__ import absolute_import
import enum

import typing as tp
from threading import Thread, Lock, Condition
from enum import Enum
from queue import Queue
from uuid import uuid1
from ctypes import *
import numpy as np

from pipeline.meta import DescriptablePool
from utils.LoggerManager import LoggerManager, bcolors

exec_logger = LoggerManager.get_logger('Executor')

class DeviceType(str, Enum):
    CPU = 'CPU'
    TKDNN = 'TKDNN'
    TORCH = 'TORCH'

class Executor(DescriptablePool):

    def __init__(self, object_name, device_type):
        super(Executor, self).__init__(object_name)
        self.device_type = device_type
    
    def process(self, *args, **kwargs):
        raise NotImplementedError(f'{self.__class__}')

class CPUExecutor(Executor):

    def __init__(self, object_name, n_workers):
        super(CPUExecutor, self).__init__(object_name, 'CPU')
        if n_workers < 1:
            raise ValueError('Must be at least 1 worker')
        self.task_queue = Queue()
        self.results = dict()
        self.cv = Condition(Lock())
        self.workers = [Thread(target=self._process, daemon=True) for _ in range(n_workers)]
        for worker in self.workers:
            worker.start()
        exec_logger.info(f'Started {self.object_name} CPU executor with {n_workers} work threads.')
        
    def _process(self):
        while True:
            function, args, kwargs, uuid = self.task_queue.get()
            result = function(*args, **kwargs)
            with self.cv:
                self.results[uuid] = result
                self.cv.notify_all()

    def process(self, function, args=tuple(), kwargs=None):
        # publish task
        if kwargs is None:
            kwargs = dict()
        task_uuid = uuid1()
        datapack = (function, args, kwargs, task_uuid)
        self.task_queue.put(datapack)
        # wait for result
        with self.cv:
            self.cv.wait_for(lambda: task_uuid in self.results)
            result = self.results[task_uuid]
            del self.results[task_uuid]
            return result

    @staticmethod
    def _from_json_dict(json: dict):
        return CPUExecutor(**json)


def to_Image(frame):
    H, W = frame.shape[:2]
    arr = np.ascontiguousarray(frame.flat, dtype=np.uint8)
    data = arr.ctypes.data_as(POINTER(c_ubyte))
    im = Image(W, H, data)
    return im


class Image(Structure):
    _fields_ = [("w", c_int),
                ("h", c_int),
                ("data", POINTER(c_ubyte))]


class Box(Structure):
    _fields_ = [("x0", c_float),
                ("y0", c_float),
                ("x1", c_float),
                ("y1", c_float),
                ("probability", c_float),
                ("class_id", c_int),
               ]


class Predictions(Structure):
    _fields_ = [("boxes", POINTER(Box)),
                ("n", c_int),
                ]


class BatchPredictions(Structure):
    _fields_ = [("predictions", POINTER(Predictions)),
                ("n", c_int),
                ]


class tkDNNExecutor(Executor):

    def __init__(self, object_name, weight_path, max_batch_size, lib_path, n_classes, car_classes, threshold):
        super(tkDNNExecutor, self).__init__(object_name, 'TKDNN')
        self.weight_path = weight_path
        self.max_batch_size = max_batch_size
        self.n_classes = n_classes
        self.car_classes = car_classes
        self.threshold = threshold

        self.lib = lib = CDLL(lib_path, RTLD_GLOBAL)

        init_net = lib.init_net
        init_net.argtypes = [c_char_p, c_int, c_int]
        self.init_net = init_net

        set_threshold = lib.set_threshold
        set_threshold.argtypes = [c_float]
        self.set_threshold = set_threshold
        
        add_image = lib.add_image
        add_image.argtypes = [Image]
        self.add_image = add_image

        predict = lib.predict
        predict.restype = BatchPredictions
        self.predict = predict

        free_predictions = lib.free_predictions
        free_predictions.argtypes = [BatchPredictions]
        self.free_predictions = free_predictions

        self.reset_buffer = lib.reset_buffer
        self.loaded = False

    def _load(self):
        self.init_net(self.weight_path.encode('utf-8'), 
                 self.max_batch_size, 
                 self.n_classes)

        self.set_threshold(self.threshold)
        exec_logger.info(f'Started {self.object_name} tkDNN executor with weights {self.weight_path} and threshold {self.threshold}')
        self.loaded = True

    def proccess(self, image_list, *args, **kwargs):
        if not self.loaded:
            self._load()

        self.reset_buffer()
        for image in image_list:
            self.add_image(to_Image(image))
        
        result = self.predict()
        
        batch_boxes = []
        for i, _ in enumerate(image_list):
            image_boxes = []
            preds = result.predictions[i]
            for j in range(preds.n):
                bbox = preds.boxes[j]
                bbox = np.array([bbox.x0, bbox.y0, bbox.x1, bbox.y1, bbox.class_id, bbox.probability]).reshape(-1, 6)
                image_boxes.append(bbox)
            batch_boxes.append(np.vstack(image_boxes) if image_boxes else np.empty((0, 6)))
        
        self.free_predictions(result)

        return batch_boxes

    @staticmethod
    def _from_json_dict(json: dict):
        return tkDNNExecutor(**json)