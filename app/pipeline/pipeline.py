from __future__ import absolute_import

import typing as tp

from pipeline.meta import DescriptablePool
from pipeline import TaskQueue

class Pipeline(DescriptablePool):

    def __init__(self, stages, executors, queues):
        super(Pipeline, self).__init__('pipeline')
        self.queues = queues
        self.stages = stages
        self.executors = executors

    def initialize(self):
        for stage in self.stages:
            stage.initialize()

    def start(self):
        for stage in self.stages:
            stage.start()

    @staticmethod
    def _from_json_dict(json: dict):
        queues = [DescriptablePool.from_json_dict(q) for q in json['queues']]
        executors = [DescriptablePool.from_json_dict(q) for q in json['executors']]
        stages = [DescriptablePool.from_json_dict(q) for q in json['stages']]
        return Pipeline(stages, executors, queues)