import json
import typing as tp

class SubscriptableClass(type):
    def __getitem__(cls, item):
        config = getattr(cls, cls.__descriptable__)
        return config[item]

class DescriptablePoolMeta(type, metaclass=SubscriptableClass):
    __descriptable__ = 'NAME2POOL'
    NAME2POOL: tp.Dict[str, tp.Dict[str, object]] = dict()

    @staticmethod
    def register_class(cls_obj, cls_name):
        DescriptablePoolMeta.NAME2POOL[cls_name] = cls_obj.NAME2OBJECT

    def __new__(cls, name, bases, attrs):
        new_cls = super(DescriptablePoolMeta, cls).__new__(cls, name, bases, attrs)
        DescriptablePoolMeta.register_class(new_cls, name)
        return new_cls

    def __init__(cls, name, bases, attrs):
        super(DescriptablePoolMeta, cls).__init__(name, bases, attrs)

    def __getitem__(cls, item):
        return DescriptablePoolMeta.NAME2POOL[cls.__name__][item]
    

class GlobalConfig(metaclass=SubscriptableClass):
    __descriptable__ = 'config_dict'
    config_dict: dict = None

    def __init__(self, cfg_path='config/config.json'):
        if GlobalConfig.config_dict is None:
            with open(cfg_path, 'r') as f:
                GlobalConfig.config_dict = json.load(f)
        else:
            raise RuntimeError('Tried to reinit global config')

    @staticmethod
    def get(field_name, default_value=None):
        return GlobalConfig.config_dict.get(field_name, default_value)

class DescriptablePool(metaclass=DescriptablePoolMeta):
    __descriptable__ = 'NAME2OBJECT'
    SUBCLASSESS = None
    NAME2OBJECT = dict()

    def __init__(self, object_name: str):
        self.object_name: str = object_name

        _pool = self.__class__.NAME2OBJECT
        if self.object_name in _pool:
            raise ValueError(f'Object {self.object_name} is already exist in system')
        _pool[self.object_name] = self


    def _to_json_dict(self) -> dict:
        raise NotImplementedError(f'Implement _to_json_dict method in {self.__class__}')

    @staticmethod
    def _from_json_dict(json: dict) -> object:
        raise NotImplementedError(f'Implement _from_json_dict method')

    @classmethod
    def _serializable_classname(cls):
        name = f'{cls.__module__}.{cls.__name__}'
        return name

    @staticmethod
    def _init_subclasses():
        if DescriptablePool.SUBCLASSESS is None:
            subclasses = dict()
            aliases = dict()
            work = [DescriptablePool]
            while work:
                parent = work.pop()
                for child in parent.__subclasses__():
                    if child not in subclasses:
                        cls_name = child._serializable_classname()
                        subclasses[cls_name] = child
                        
                        alias = getattr(child, '__alias__', cls_name.split('.')[-1])
                        if alias in aliases:
                            raise RuntimeError(f'{cls_name} have conflicting alias!')
                        aliases[alias] = child

                        work.append(child)
            if '<unknown>.BackendType' in subclasses:
                subclasses['entities.cameras.CameraManager.BackendType'] = subclasses['<unknown>.BackendType']
            elif 'entities.cameras.CameraManager.BackendType' in subclasses:
                subclasses['<unknown>.BackendType'] = subclasses['entities.cameras.CameraManager.BackendType']

            DescriptablePool.SUBCLASSESS = {**subclasses, **aliases}
            # print(DescriptablePool.SUBCLASSESS.keys())
            # exit(0)

    def to_json_dict(self) -> dict:
        DescriptablePool._init_subclasses()
        config = self._to_json_dict()
        config['classname'] = self._serializable_classname()
        return config

    @staticmethod
    def from_json_dict(json: dict) -> object:
        DescriptablePool._init_subclasses()
        classname = json['classname']
        del json['classname']
        try:
            return DescriptablePool.SUBCLASSESS[classname]._from_json_dict(json)
        except:
            print(f'Couldnot deserialize {classname}')
            raise