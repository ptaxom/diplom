from __future__ import absolute_import

import typing as tp
import numpy as np
import cv2

from pipeline import Task
from pipeline.meta import SubscriptableClass, DescriptablePool
from pipeline.stages import Renderer, Tracklet


class Line(DescriptablePool):

    def __init__(self, object_name: str, coords: tp.List[float], window_size: int = 5):
        super().__init__(object_name)
        self.coords = coords
        x1, y1, x2, y2 = coords
        self.A = y2 - y1
        self.B = x1 - x2
        self.C = -(x1 * y2 - x2 * y1)
        self.processed_objects = set()
        self.window_size = window_size

    def check_bbox(self, bbox):
        bbox = bbox.reshape(-1, 4)
        x = bbox[:, ::2].mean(axis=1)
        y = bbox[:, 1::2].mean(axis=1)

        return self.check_point(x, y)

    def check_point(self, x, y):
        return (self.A * x + self.B * y + self.C) < 0

    def decision_boundary(self, image):
        H, W = image.shape[:2]
        x = np.linspace(0, 1, W)
        y = np.linspace(0, 1, H)
        xv, yv = np.meshgrid(x, y)
        inside = self.check_point(xv, yv)
        return inside
    
    def render_line(self, image):
        H, W = image.shape[:2]
        points = np.multiply(self.coords, [W, H, W, H]).astype('int')
        cv2.line(image, tuple(points[:2]), tuple(points[2:]), (255, 0, 0), 5)

        r_point = tuple(np.maximum(points[:2] - 30, 0))
        cv2.putText(image, f'#{len(self.processed_objects)}', r_point, 
            cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 0), 1)


    def check_obj(self, obj_id, limit=None):
        if obj_id in self.processed_objects:
            return
        
        ws = self.window_size
        obj = Tracklet[obj_id]
        
        if limit is not None:
            frames = sorted([x for x in obj.history if x <= limit])
        else:
            frames = sorted(list(obj.history.keys()))

        if len(frames) < 2 * ws:
            return

        in_begin = np.vstack([obj.history[idx][0] for idx in frames[:ws]])
        in_end = np.vstack([obj.history[idx][0] for idx in frames[-ws:]])

        control_sing = self.check_bbox(in_begin[0])
        if np.all(self.check_bbox(in_begin) == control_sing) and np.all(self.check_bbox(in_end) != control_sing):
            self.processed_objects.add(obj_id)        

    @staticmethod
    def _from_json_dict(json: dict) -> object:
        return Line(**json)


class ObjectCounter(Renderer):

    def __init__(self, object_name: str, lines: tp.List[Line], render_boundary=False,  render_line: bool = True, cls='cars'):
        super().__init__(object_name)
        self.obj_cls = cls
        self.lines = lines
        self.render_boundary = render_boundary
        self.render_line = render_line

    def render(self, tasks: tp.List[Task]) -> tp.List[Task]:
        for task in tasks:
            limit = None
            image = task.data['view_image']

            if self.render_boundary:
                
                canvas = np.zeros_like(image)

                boundary = self.lines[0].decision_boundary(image)
                for line in self.lines[1:]:
                    boundary ^= line.decision_boundary(image)
                canvas[...,2] = boundary * 255.
                canvas[...,1] = (1 - boundary) * 255.

                image = (image * 0.8 + canvas * 0.2).astype('uint8')

            if self.render_line:
                for line in self.lines:
                    line.render_line(image)
                    limit = task.order_id

            task.data['view_image'] = image

            for line in self.lines:
                for obj_id in task.data[self.obj_cls]['current_objects']:
                    line.check_obj(obj_id, limit)

        return tasks

    @staticmethod
    def _from_json_dict(json: dict) -> object:
        json['lines'] = [DescriptablePool.from_json_dict(x) for x in json['lines']]
        return ObjectCounter(**json)