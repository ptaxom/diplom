from __future__ import absolute_import

import typing as tp
import numpy as np
from numba import jit
from numba.core.errors import NumbaWarning
import warnings
warnings.simplefilter('ignore', category=NumbaWarning)

from pipeline import Task
from pipeline.stages.processing_stages.processors import Postprocessor


@jit(fastmath=True)
def nms(bboxes: np.ndarray, nms_threshold: float) -> np.ndarray:
    EPS:float = 1e-4
    pick = []
	
    x1 = bboxes[:,0]
    y1 = bboxes[:,1]
    x2 = bboxes[:,2]
    y2 = bboxes[:,3]

    area = (x2 - x1 + EPS) * (y2 - y1 + EPS)
    idxs = np.argsort(y2)

    while len(idxs) > 0:
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
    
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])
    
        w = np.maximum(0, xx2 - xx1 + EPS)
        h = np.maximum(0, yy2 - yy1 + EPS)
    
        overlap = (w * h) / area[idxs[:last]]
    
        idxs = np.delete(idxs, np.concatenate(([last],
        np.where(overlap > nms_threshold)[0])))
    
    return bboxes[pick].astype("float32")


class NMSProcessor(Postprocessor):

    def __init__(self, object_name: str, nms_theshold: float = 0.5, probability_threshold: float = 0.5):
        super().__init__(object_name)
        self.nms_theshold = nms_theshold
        self.probability_threshold = probability_threshold

    def postprocess(self, tasks: tp.List[Task]) -> tp.List[Task]:
        for task in tasks:
            data = task.data['detections']
            cls_bboxes = []
            conf_bboxes = data[np.where(data[:, -1] > self.probability_threshold)][:, :5]
            classes = set(list(conf_bboxes[:, -1]))
            
            for cls in classes:
                cls_dets = conf_bboxes[np.where(conf_bboxes[:, -1].astype('int') == int(cls))]
                if cls_dets.shape[0] == 0:
                    bboxes = cls_dets
                else:
                    bboxes = nms(cls_dets, self.nms_theshold)

                cls_bboxes.append(bboxes)

            task.data['detections'] = np.vstack(cls_bboxes or [np.empty((0, 5))])

        return tasks

    @staticmethod
    def _from_json_dict(json: dict):
        return NMSProcessor(**json)