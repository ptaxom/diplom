from __future__ import absolute_import

import pandas as pd
import typing as tp
import numpy as np
import os


from pipeline.stages import Postprocessor
from pipeline import Task
from settings import DEBUG
from utils.monitoring import log_task_metrics

class RedisLogger(Postprocessor):

    def __init__(self, object_name: str):
        super().__init__(object_name)
        self.key = f'{object_name}_metrics'


    def postprocess(self, tasks: tp.List[Task]) -> tp.List[Task]:
        for task in tasks:
            log_task_metrics(self.key, task)
        return tasks

    @staticmethod
    def _from_json_dict(json: dict):
        return RedisLogger(**json)


class DetectionsLogger(Postprocessor):

    def __init__(self, object_name: str, csv_path: str, column_tags: tp.List[str], save_every: int=100):
        super().__init__(object_name)
        self.csv_path = os.path.join('../logs/', csv_path)
        self.iteration = 0
        self.column_tags = column_tags
        self.columns = ['frame_id', *column_tags, 'x0', 'y0', 'x1', 'y1', 'class_id', 'probability']
        self.df = pd.DataFrame(columns=self.column_tags)
        self.save_every = save_every

    def postprocess(self, tasks: tp.List[Task]) -> tp.List[Task]:
        total_data = []
        for task in tasks:
            data = np.array([None] * len(self.columns))
            data[0] = task.order_id
            for i, tag in enumerate(self.column_tags):
                data[1 + i] = task.tags.get(tag)
            for bbox in task.data['detections']:
                row = data.copy()
                row[-6:] = bbox.reshape(-1)
                total_data.append(row)
        
        total_data = np.vstack(total_data or [np.empty((0, len(self.columns)))])
        df = pd.DataFrame(data=total_data, columns=self.columns)
        self.df = self.df.append(df)

        self.iteration += 1
        if self.iteration % self.save_every == 0:
            self.df.to_csv(self.csv_path, index=False)

        return tasks

    @staticmethod
    def _from_json_dict(json: dict):
        return DetectionsLogger(**json)