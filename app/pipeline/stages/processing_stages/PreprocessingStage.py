from __future__ import absolute_import

import typing as tp


from pipeline.stages import PipelineStage
from pipeline import Task, Executor
from pipeline.meta import DescriptablePool
from utils.monitoring import TimeEstimator

class PreprocessingStage(PipelineStage):

    def __init__(self, object_name, input_queue, target_queues, preprocessor, executor_name='default'):
        super().__init__(object_name, input_queue, target_queues, executor_name)
        self.preprocessor = preprocessor

    def process_data(self, tasks: tp.List[Task]) -> tp.List[Task]:
        executor = Executor[self.executor]
        result = executor.process(self.preprocessor.preprocess, (tasks,))
        return result

    @staticmethod
    def _from_json_dict(json: dict):
        json['preprocessor'] = DescriptablePool.from_json_dict(json['preprocessor'])
        return PreprocessingStage(**json)