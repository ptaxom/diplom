from __future__ import absolute_import

import typing as tp


from pipeline.stages import PipelineStage
from pipeline import Task
from pipeline.meta import DescriptablePool


class Preprocessor(DescriptablePool):

    def preprocess(self, tasks: tp.List[Task]) -> tp.List[Task]:
        raise NotImplementedError(f'preprocess not implemented {self.__class__}')

class Postprocessor(DescriptablePool):

    def postprocess(self, tasks: tp.List[Task]) -> tp.List[Task]:
        raise NotImplementedError(f'postprocess not implemented {self.__class__}')

class Renderer(Postprocessor):

    def render(self, tasks: tp.List[Task]) -> tp.List[Task]:
        raise NotImplementedError(f'render not implemented {self.__class__}')

    def postprocess(self, tasks: tp.List[Task]) -> tp.List[Task]:
        for task in tasks:
            image = task.data['image']
            task.data['view_image'] = task.data.get('view_image', image.copy())

        return self.render(tasks)


class NestedProcessor(Postprocessor):

    def __init__(self, object_name: str, processors: tp.List[Postprocessor]):
        super().__init__(object_name)
        self.processors = processors

    def postprocess(self, tasks: tp.List[Task]) -> tp.List[Task]:
        result = tasks
        for processor in self.processors:
            result = processor.postprocess(result)
        return result

    @staticmethod
    def _from_json_dict(json: dict) -> object:
        processors = [DescriptablePool.from_json_dict(x) for x in json['processors']]
        return NestedProcessor(json['object_name'], processors)