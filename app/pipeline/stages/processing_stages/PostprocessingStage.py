from __future__ import absolute_import
from ctypes import resize

import typing as tp


from pipeline.stages import PipelineStage
from pipeline import Task, Executor
from pipeline.meta import DescriptablePool

class PostprocessingStage(PipelineStage):

    def __init__(self, object_name, input_queue, target_queues, postpocessor, executor_name='default'):
        super().__init__(object_name, input_queue, target_queues, executor_name)
        self.postpocessor = postpocessor

    def process_data(self, tasks: tp.List[Task]) -> tp.List[Task]:
        executor = Executor[self.executor]
        result = executor.process(self.postpocessor.postprocess, (tasks,))
        return result

    @staticmethod
    def _from_json_dict(json: dict):
        json['postpocessor'] = DescriptablePool.from_json_dict(json['postpocessor'])
        return PostprocessingStage(**json)