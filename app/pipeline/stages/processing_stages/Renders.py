from __future__ import absolute_import

import typing as tp
import cv2
import os
import numpy as np
from threading import Thread, Lock
from queue import Queue
import time

from pipeline.stages import Renderer, Tracklet
from pipeline import Task
from settings import DEBUG
from utils.visutils import plot_bboxes_id_color

RENDER_QUEUE = Queue(maxsize=50)
STOPPED = False
N_WINDOW = 0

def render_worker():
    global STOPPED
    iteration = 0
    while not STOPPED:
        window_name, image = RENDER_QUEUE.get()
        if not DEBUG:
            image = np.uint8(image)
            cv2.imshow(window_name, image)
            STOPPED = cv2.waitKey(1) == ord('q')
            # iteration += 1
            # if iteration % N_WINDOW == 0:
    print('STOPPED')

def render_image(window_name, image):
    RENDER_QUEUE.put((window_name, image))

WORKER_LOCK = Lock()
WORKER = None

class DetectionsRenderer(Renderer):

    def __init__(self, object_name, classes_colors: tp.Dict[str, tp.Tuple[int]] = None):
        super().__init__(object_name)
        self.classes_colors = classes_colors

    def render(self, tasks: tp.List[Task]) -> tp.List[Task]:
        for task in tasks:
            image = task.data['view_image']
            if self.classes_colors is None:
                plot_bboxes_id_color(image, task.data['detections'][:, :4])
            else:
                for cls, default_color in self.classes_colors.items():
                    detections = task.data[cls]['t_dets']
                    plot_bboxes_id_color(image, detections,default_color=tuple(default_color))
        return tasks

    @staticmethod
    def _from_json_dict(json: dict):
        return DetectionsRenderer(**json)

class TrajectoryRenderer(Renderer):

    def __init__(self, object_name: str, classes_colors: tp.Dict[str, tp.Tuple[int]]):
        super().__init__(object_name)
        self.classes_colors = classes_colors

    def render(self, tasks: tp.List[Task]) -> tp.List[Task]:
        for task in tasks:
            image = task.data['view_image']
            for cls, default_color in self.classes_colors.items():
                obj_ids = task.data[cls]['current_objects']
                for obj_id in obj_ids:
                    obj = Tracklet[obj_id]
                    trajectory = np.vstack([obj.history[k][0] for k in obj.history if k <= task.order_id] or [np.empty((0, 4))])
                    for bbox in trajectory:
                        x_c = int(bbox[::2].mean() * image.shape[1])
                        y_c = int(bbox[1::2].mean() * image.shape[0])
                        cv2.circle(image, (x_c, y_c), 5, tuple(default_color), -1)

        return tasks

    @staticmethod
    def _from_json_dict(json: dict):
        return TrajectoryRenderer(**json)

class WindowRenderer(Renderer):

    def __init__(self, object_name, window_name, width=None, height=None):
        super().__init__(object_name)
        self.window_name = window_name
        self.width = width
        self.height = height
        self.need_reshape = all([width, height])
        
        global N_WINDOW, WORKER
        N_WINDOW += 1
        with WORKER_LOCK:
            if WORKER is None:
                WORKER = Thread(target=render_worker, daemon=True)
                WORKER.start()

    def render(self, tasks: tp.List[Task]) -> tp.List[Task]:
        for task in tasks:
            image = task.data['view_image']
            if self.need_reshape:
                image = cv2.resize(image, (self.width, self.height)).astype('uint8')

            render_image(self.window_name, image)
            if STOPPED:
                raise RuntimeError('Pipeline was stopped')
        return tasks

    @staticmethod
    def _from_json_dict(json: dict):
        return WindowRenderer(**json)

class FileRenderer(Renderer):

    def __init__(self, object_name: str, filename, fourcc='MPEG'):
        super().__init__(object_name)
        self.path = os.path.join('../logs/', filename)
        self.fourcc = cv2.VideoWriter_fourcc(*fourcc)
        
        self.camera_name = None
        self.writer = None

    def initialize(self, image):
        self.shape = image.shape[:2]
        self.writer = cv2.VideoWriter(self.path, self.fourcc, 10, (self.shape[::-1]))
        

    def render(self, tasks: tp.List[Task]) -> tp.List[Task]:
        cameras = list({task.tags['cam_name'] for task in tasks})
        
        if len(cameras) > 1:
            raise RuntimeError(f'{self.object_name}: Could not save multiple cameras in one file')

        if self.camera_name is None:
            self.camera_name = cameras[0]

        if self.camera_name != cameras[0]:
            raise RuntimeError(f'{self.object_name}: Could not save {cameras[0]} to file, which already recording {self.camera_name}')

        for task in tasks:
            image = task.data['view_image']
            if self.writer is None:
                self.initialize(image)

            if image.shape[:2] != self.shape:
                raise RuntimeError(f'{self.object_name}: Could not save {image.shape[:2]} shape to file, which already recording {self.shape}')
                
            self.writer.write(image)

        return tasks

    @staticmethod
    def _from_json_dict(json: dict) -> object:
        return FileRenderer(**json)