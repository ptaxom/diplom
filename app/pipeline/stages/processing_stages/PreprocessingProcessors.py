from __future__ import absolute_import

import typing as tp
import warnings

import numpy as np
import cv2
from numba import jit
from numba.core.errors import NumbaWarning

warnings.simplefilter('ignore', category=NumbaWarning)

from pipeline import Task
from pipeline.stages.processing_stages.processors import Preprocessor


class LensFun:
    def __init__(self, height: int = 416, input_width: int = 740):
        self.h, self.w = height, input_width
        self.x = (self.w - self.h) / 2
        self.y = 0
        self.undist_coords = np.load(f'resources/LensFunUndistCoords_{self.h}.npy')

    def transform(self, frame):
        undist_frame = frame.copy()
        undist_frame = cv2.resize(undist_frame, (self.w, self.h))
        undist_frame = cv2.remap(
            undist_frame, self.undist_coords, None, cv2.INTER_LINEAR)
        undist_frame = undist_frame[int(self.y):int(self.y+self.h),
                                    int(self.x):int(self.x+self.h)]

        return undist_frame


class DefisheyeProcessor(Preprocessor):
    def __init__(self, object_name: str, height: int = 416, input_width: int = 740):
        super().__init__(object_name)
        self.engine = LensFun(height, input_width)

    def preprocess(self, tasks: tp.List[Task]) -> tp.List[Task]:
        for task in tasks:
            frame = task.data['image']
            undist_frame = self.engine.transform(frame)
            task.data['image'] = undist_frame

        return tasks

    @staticmethod
    def _from_json_dict(json: dict):
        return DefisheyeProcessor(**json)
