from __future__ import absolute_import

import cv2
import typing as tp
import pandas as pd
from pipeline.queue import TaskQueue

from pipeline.stages import PipelineStage
from pipeline import Task, DeviceType, tkDNNExecutor, Executor
from utils.LoggerManager import LoggerManager, bcolors

detstage_logger = LoggerManager.get_logger('Detection stage logger')


class DetectionStage(PipelineStage):

    def __init__(self, input_queue, target_queues, executor_name, dummy_path=None):
        super().__init__('DetectionStage', input_queue, target_queues, executor_name)
        try:
            self.precomputed_df = pd.read_csv(dummy_path)
        except:
            self.precomputed_df = None
            if dummy_path is not None:
                detstage_logger.critical(f'Tried to load {dummy_path}, but with error. Using inference engine')

    def initialize(self):
        if self.precomputed_df is not None:
            self.initialized = True
            return

        self.executor: tkDNNExecutor = Executor[self.executor]
        assert self.executor.device_type == DeviceType.TKDNN, 'Only possible to use DetectionStage with tkDNN executor'
        assert self.executor.max_batch_size == TaskQueue[self.input_queue].max_batch_size, 'tkDNN max batch size must be equal queue batch size'
        self.initialized = True

    def process_data(self, tasks: tp.List[Task]):
        # print(self.object_name, ' '.join(map(lambda x: str(x.order_id), tasks)))
        if self.precomputed_df is None:
            images = [task.data['image'] for task in  tasks]
            batch_detections = self.executor.proccess(images)

            for i, batch_detection in enumerate(batch_detections):
                tasks[i].data['detections'] = batch_detection
        else:
            df = self.precomputed_df
            for task in tasks:
                detections = df.loc[(df['frame_id'] == task.order_id) & (df['cam_name'] == task.tags['cam_name'])].values[:,-6:]
                task.data['detections'] = detections

        return tasks

    @staticmethod
    def _from_json_dict(json: dict):
        return DetectionStage(**json)