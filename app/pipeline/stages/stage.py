from __future__ import absolute_import

import typing as tp
from threading import Thread

from pipeline.meta import DescriptablePool
from pipeline.task import Task
from pipeline.executors import Executor
from pipeline.queue import TaskQueue

from utils.LoggerManager import LoggerManager
from utils.monitoring import TimeEstimator

stage_logger = LoggerManager.get_logger('PipelineStage')

class PipelineStage(DescriptablePool):

    def __init__(self, object_name, input_queue, target_queues, executor_name):
        super(PipelineStage, self).__init__(object_name)
        self.input_queue: str = input_queue
        self.target_queues:  tp.List[str] = target_queues
        self.executor: Executor = executor_name
        self.initialized: bool = False
        self.worker_thread: Thread = Thread(target=self.process, daemon=True)

    def initialize(self):
        self.initialized = True
    
    def start(self):
        if not self.initialized:
            raise RuntimeError(f'Not initialized stage {self.object_name}')
        self.worker_thread.start()
    
    def process_data(self, tasks: tp.List[Task]) -> tp.List[Task]:
        raise NotImplementedError(f'{self.__class__} process_data')

    def process(self):
        # TODO: fix this by event and create gracefull shutdown
        while True:
            try:                    
                tasks = TaskQueue[self.input_queue].pop_data()
                with TimeEstimator(self.object_name):
                    processed_data = self.process_data(tasks)
                for target_queue in self.target_queues:
                    if target_queue is not None:
                        if not isinstance(processed_data, (list, tuple)):
                            processed_data = (processed_data, )

                        for data_chunk in processed_data:
                            TaskQueue[target_queue].push_data(data_chunk.copy())
            except:
                stage_logger.error(f'Exception in {self.object_name}', exc_info=True)
                raise

