from __future__ import absolute_import

import typing as tp
import numpy as np
from threading import Lock

from pipeline.stages import PipelineStage
from pipeline import Task
from pipeline.meta import DescriptablePool
from pipeline.stages.tracking.Tracker import Tracker


class TrackingStage(PipelineStage):

    def __init__(self, object_name, input_queue, target_queues, tracker_builder, trackable_groups, executor_name='default'):
        super().__init__(object_name, input_queue, target_queues, executor_name)
        self.trackable_groups: tp.Dict[str, tp.List[int]] = trackable_groups
        self.trackers: tp.Dict[str, Tracker] = {group: tracker_builder() for group in trackable_groups}

    def process_data(self, tasks: tp.List[Task]) -> tp.List[Task]:
        for task in tasks:
            detections, image = [task.data[x] for x in ['detections', 'image']]
            results = dict()
            
            for group, class_ids in self.trackable_groups.items():
                group_detections = detections[np.where(np.isin(detections[:,-1], class_ids))][:, :4]
                tracker = self.trackers[group]
                results[group] = tracker.update(group, task.order_id, task.tags['cam_name'], group_detections, image)

            task.data = {**task.data, **results}
        return tasks

    @staticmethod
    def _from_json_dict(json: dict):
        data = json['tracker'].copy()
        json['tracker_builder'] = lambda: DescriptablePool.from_json_dict(data)
        del json['tracker']
        return TrackingStage(**json)