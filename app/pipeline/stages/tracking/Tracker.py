from __future__ import absolute_import

import typing as tp
import numpy as np
from threading import Lock

from pipeline import Task
from pipeline.meta import SubscriptableClass, DescriptablePool


class Tracklet(metaclass=SubscriptableClass):
    __descriptable__ = 'DATABASE'

    DELETE_AFTER = 60
    DATABASE = dict()
    ID = 0
    ETA = 0.2
    LOCK = Lock()

    def __init__(self, group, order_id: int, cam_name: str, bbox: np.ndarray, image: np.ndarray) -> None:
        with Tracklet.LOCK:
            self.id = Tracklet.ID
            self.group = group
            self.name = f'{group}_{order_id}_{cam_name}_{self.id}'
            Tracklet.ID += 1
            Tracklet.DATABASE[self.name] = self
        self.bbox = bbox
        self.history = dict()
        self.update(order_id, bbox, image)

    def update(self, order_id:int, bbox: np.ndarray, image: np.ndarray):
        ETA = Tracklet.ETA
        self.bbox = self.bbox * ETA + (1 - ETA) * bbox
        self.bbox = self.bbox.reshape(4)
        
        H, W = image.shape[:2]
        x0, y0, x1, y1 = np.multiply(self.bbox, [H, W, H, W]).astype('int')
        image_crop = image[y0:y1, x0:x1].copy()
        
        self.t_det = np.array([*self.bbox, self.id]).reshape(1, 5)
        self.bbox = self.bbox.reshape(1, 4)

        self.history[order_id] = (self.bbox.copy(), image_crop)
        self.not_updated = 0

    def finilize(self):
        del self.history
        del Tracklet.DATABASE[self.name]

class Tracker(DescriptablePool):

    def __init__(self, object_name: str, delete_threshold):
        super().__init__(object_name)
        self.delete_threshold = delete_threshold

        self.current_ids = set()
        self.disappeared_ids = set()
        self.to_remove = set()

    def update_disappeared(self):
        for id in list(self.disappeared_ids):
            Tracklet[id].not_updated += 1
            if Tracklet[id].not_updated > self.delete_threshold:
                self.disappeared_ids.remove(id)
                if id in self.current_ids:
                    self.current_ids.remove(id)
                    self.to_remove.add(id)
        
        for id in list(self.to_remove):
            Tracklet[id].not_updated += 1
            if Tracklet[id].not_updated > Tracklet.DELETE_AFTER:
                Tracklet[id].finilize()



    def update(self, group, order_id: int, cam_name: str, detections: np.ndarray, image: np.ndarray) -> tp.Dict[str, tp.List[str]]:
        raise NotImplementedError(f'update in {self.__class__}')


class CentroidTracker(Tracker):

    def __init__(self, object_name: str, delete_threshold: int=10, match_threshold=0.1):
        super().__init__(object_name, delete_threshold)
        self.match_threshold = match_threshold

    def update(self, group, order_id: int, cam_name: str, detections: np.ndarray, image: np.ndarray) -> tp.Dict[str, tp.List[str]]:
        all_ids = list({*self.disappeared_ids, *self.current_ids})
        new_ids = []

        matched_bboxes = []
        matched_ids = []

        for i, bbox in enumerate(detections):
            x_c = bbox[::2].mean()
            y_c = bbox[1::2].mean()

            current_bboxes = [Tracklet[x].bbox for x in all_ids]
            if not current_bboxes:
                continue

            stale_dets = np.vstack(current_bboxes)
            X_c = stale_dets[:, ::2].mean(axis=1).reshape(-1, 1)
            Y_c = stale_dets[:, 1::2].mean(axis=1).reshape(-1, 1)
            dists = np.sum(np.hstack([X_c - x_c, Y_c - y_c]) ** 2, axis=1)
            matched_id = dists.argmin()
            if dists[matched_id] < self.match_threshold:
                idx = all_ids[matched_id]
                
                all_ids.pop(matched_id)
                matched_ids.append(idx)
                matched_bboxes.append(i)

                Tracklet[idx].update(order_id, bbox, image)

        for i, bbox in enumerate(detections):
            if i not in matched_bboxes:
                vehicle = Tracklet(group, order_id, cam_name, bbox, image)
                new_ids.append(vehicle.name)

        self.current_ids = set(new_ids + matched_ids)
        self.disappeared_ids = set(all_ids)
        self.update_disappeared()

        return {
            'new_objects': new_ids,
            'current_objects': list(self.current_ids),
            'disappeared_objects': list(self.disappeared_ids),
            't_dets': np.vstack([Tracklet[x].t_det for x in self.current_ids] or [np.empty((0, 5))])
        }


    @staticmethod
    def _from_json_dict(json: dict):
        return CentroidTracker(**json)