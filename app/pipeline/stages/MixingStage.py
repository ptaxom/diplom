from __future__ import absolute_import

import typing as tp


from pipeline.stages import PipelineStage, stage_logger
from pipeline import Task, TaskQueue, queue
from utils.monitoring import TimeEstimator

from queue import Queue
from threading import Thread


class MixingStage(PipelineStage):

    def __init__(self, object_name, input_queues, target_queues, executor_name):
        super().__init__(object_name, None, target_queues, executor_name)
        self.input_queues = input_queues
        self.tasks_queue = Queue()
        self.consumers = [Thread(target=self.consume, daemon=True, args=(queue_name,)) for queue_name in input_queues]

    def initialize(self):
        self.initialized = all(map(lambda x: TaskQueue[x].max_batch_size == 1, self.input_queues))

    def start(self):
        if not self.initialized:
            raise RuntimeError(f'Not initialized stage {self.object_name}')
        for consumer in self.consumers:
            consumer.start()
        self.worker_thread.start()

    def consume(self, queue_name):
        while True:
            task = TaskQueue[queue_name].pop_data()[0]
            self.tasks_queue.put(task)

    def process(self):
        # TODO: fix this by event and create gracefull shutdown
        while True:
            try:
                with TimeEstimator(self.object_name):
                    task = self.tasks_queue.get()
                    for target_queue in self.target_queues:
                        if target_queue is not None:
                            TaskQueue[target_queue].push_data(task.copy())
            except:
                stage_logger.error(f'Exception in {self.object_name}', exc_info=True)
                raise