from __future__ import absolute_import

from utils.LoggerManager import LoggerManager

import cv2
import numpy as np
import multiprocessing as mp
import time

INIT_DELAY = 10.

gcapture_logger = LoggerManager.get_logger('GCapture')


class GCaptureWrapper(mp.Process):

    def __init__(self, url, queue, init_event, disable_event, fps):
        super().__init__()
        self.url = url
        self.pipeline = f'rtspsrc location={url} ! decodebin ! videoconvert ! appsink max-buffers=1 drop=true'
        self.queue = queue
        self.init_event = init_event
        self.disable_event = disable_event
        self.sleep_time = max(1. / fps - 0.01, 0.01)

    def run(self):
        self.capture = cv2.VideoCapture(self.pipeline, cv2.CAP_GSTREAMER)
#         self.queue.put(self.capture.isOpened())
        self.init_event.set()
        while not self.disable_event.is_set():
            ready, frame = self.capture.read()
            if ready:
                if self.queue.full():
                    self.queue.get()
                self.queue.put(frame)
            time.sleep(self.sleep_time)

        self.capture.release()

        gcapture_logger.info('Released')


class GCapture:

    def __init__(self, url, fps):
        self.queue = mp.Queue(maxsize=2)
        self.event = mp.Event()
        init_event = mp.Event()
        self.process = GCaptureWrapper(url, self.queue, init_event, self.event, fps)
        self.process.start()

        initialized = init_event.wait(INIT_DELAY + 1.)
#         initialized &= self.queue.pop()
        if not initialized:
            raise RuntimeError(f'Cannot open rtsp url: {url}')
        self.read()
        gcapture_logger.debug(f'Using GCapture for {url}')

    def _check(self):
        if self.queue.empty():
            return False, None
        val = self.queue.get()
        if val is not None:
            return True, val

    def read(self):
        t0 = time.time()
        while time.time() - t0 < 5:
            ready, frame = self._check()
            if ready:
                break
            time.sleep(0.01)
        return ready, frame

    def release(self):
        self.event.set()
        gcapture_logger.debug('Setted event')
        for _ in range(5):
            try:
                if self.queue.empty():
                    gcapture_logger.debug('queue empty')
                    break
                _ = self.queue.get()
            except:
                break
            self.queue.close()
            gcapture_logger.debug('queue closed')
            break
        self.process.join(5)
        gcapture_logger.debug('Joined')

    def isOpened(self):
        return True