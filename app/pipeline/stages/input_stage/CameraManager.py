from __future__ import absolute_import

from pipeline.meta import DescriptablePool
from pipeline.stages.input_stage.GCapture import GCapture
from pipeline.stages.input_stage.FileCapture import FileVideoStream

from utils.LoggerManager import LoggerManager

import typing as tp
import time
import numpy as np
import cv2

USE_GSTREAMER = False
camera_manager_logger = LoggerManager.get_logger('CameraManager')


class BackendType(DescriptablePool):
    OPENCV = "OPENCV"
    OPENCV_PARALLEL = "OPENCV_PARALLEL"
    GSTREAMER = "GSTREAMER"

    val_dict = dict()

    def __init__(self, value):
        if not BackendType.val_dict:
            for attr, val in BackendType.__dict__.items():
                if isinstance(val, str):
                    BackendType.val_dict[val] = attr

        if isinstance(value, BackendType):
            self.value = value
        elif isinstance(value, str):
            self.value = BackendType.val_dict[value]
        else:
            raise ValueError('Invalid value')

    def _to_json_dict(self):
        return {'TYPE': self.value}

    @staticmethod
    def _from_json_dict( json: dict):
        backend_type = BackendType(json['TYPE'])
        if backend_type == BackendType.GSTREAMER:
            USE_GSTREAMER = True
        return backend_type.value

class CodecType(DescriptablePool):
    H264 = 0
    H265 = 1
    MJPEG = 2

    val_dict = dict()

    def __init__(self, value):
        if not BackendType.val_dict:
            for attr, val in BackendType.__dict__.items():
                if isinstance(val, int):
                    BackendType.val_dict[val] = attr

        if isinstance(value, BackendType):
            self.value = value
        elif isinstance(value, int):
            self.value = BackendType.val_dict[value]
        else:
            raise ValueError('Invalid value')

    def _to_json_dict(self):
        return {'TYPE': self.value}

    @staticmethod
    def _from_json_dict( json: dict):
        backend_type = CodecType(json['TYPE'])
        return backend_type.value

class CameraManager(DescriptablePool):
    TOTAL_FRAMES = 10**10
    CAM_NAMES = []
    WIDTH = None
    HEIGHT = None
    FPS = 10
    __descriptable__ = 'VIEWS'
    VIEWS = dict()
    
    def __init__(self, name2source: tp.Dict[str, tp.Any], backend_type: BackendType, 
            input_masks_pathes: tp.Dict[str, str] = dict(),
            width: int = 1280, height: int = 768,
            is_h264_encoded: tp.Dict[str, int] = dict(),
            skip_frames:int = 0,
            skip_begin: int = 0,
            fps = None
            ):
            super(CameraManager, self).__init__(None)

            self.name2source: tp.Dict[str, tp.Any] = name2source
            self.backend_type = backend_type
            CameraManager.WIDTH = width
            CameraManager.HEIGHT = height
            CameraManager.FPS = fps
            self.width: int = width
            self.height:int = height
            self.input_masks_pathes: tp.Dict[str, str] = input_masks_pathes
            self.load_masks(input_masks_pathes)
            self.backend: CameraBackend = None

            if backend_type in [BackendType.OPENCV, BackendType.OPENCV_PARALLEL]:
                self.backend = OpenCVBackend(name2source, backend_type, width, height, skip_frames=skip_frames)
            else:
                self.backend = GStreamerBackend(name2source, backend_type, width, height, is_h264_encoded)
    
            if backend_type != BackendType.GSTREAMER and skip_begin != 0:
                self.backend.set_frame(skip_begin)

            CameraManager.CAM_NAMES = [x for x in self.name2source.keys()]

    
    def update_view(self):
        backend = self.backend
        backend.update_view()
        for name, view in backend.name2view.items():
            backend.name2view[name] = view & self.input_masks[name]
        
        CameraManager.VIEWS = backend.name2view

    def release(self):
        self.backend.release()

    def set_frame(self, frame_num):
        self.backend.set_frame(frame_num)

    def _to_json_dict(self):
        return {
            'sources': self.name2source,
            'input_masks_pathes': self.input_masks_pathes,
            'backend_type': self.backend_type.to_json_dict(),
            'width': self.width,
            'height': self.height
        }

    @staticmethod
    def _from_json_dict(json: dict):
        backend_type = DescriptablePool.from_json_dict(json['backend_type'])

        from pipeline.stages.input_stage.CGCapture import get_encoding # Loading after we know backend type
        sources = json['sources']
        encodings = json.get('is_h264_encoded', dict())
        if backend_type == BackendType.GSTREAMER:
            for cam_name in sources.keys():
                if cam_name not in encodings:
                    encodings[cam_name] = get_encoding(sources[cam_name])

        return CameraManager(sources, backend_type, 
                json.get('input_masks_pathes', dict()),
                json['width'], json['height'],
                encodings,
                fps=json.get('fps', 10),
                skip_begin=json.get('skip_begin', 0),
                skip_frames=json.get('skip_frames', 1)
                )

    def load_masks(self, input_masks_pathes: tp.Dict[str, str]):
        H, W = self.height, self.width
        default_mask = np.ones((H, W, 3), dtype=np.uint8) * 255
        self.input_masks: tp.Dict[str, np.ndarray] = dict()
        for name in self.name2source.keys():
            if name in input_masks_pathes:
                mask_path = input_masks_pathes[name]
                mask = cv2.imread(mask_path)
                mask = cv2.resize(mask, (W, H))
                mask = np.uint8((mask > 0) * 255)
            else:
                mask = np.copy(default_mask)
            
            self.input_masks[name] = mask


class CameraBackend:

    def __init__(self, name2source: tp.Dict[str, tp.Any], width: int = 1280, height: int = 768):
        self.name2source: tp.Dict[str, tp.Any] = name2source
        self.width: int = width
        self.height: int = height
        self.name2view: tp.Dict[str, np.ndarray] = dict()

    def update_view(self):
        raise NotImplementedError(f"Implement update_view in {self.__class__}")

    def set_frame(self, frame_num):
        raise NotImplementedError(f"Implement set_frame in {self.__class__}")

    def release(self):
        raise NotImplementedError(f"Implement release in {self.__class__}")


def create_capture(source: tp.Any, rtsp_capture: tp.Any):
    """
        Create most relevant capture for source
    """
    if isinstance(source, int):
        return cv2.VideoCapture(source)
    if isinstance(source, str):
        if 'rtsp' in source:
            return rtsp_capture(source)
        else:
            cap = FileVideoStream(source)
            CameraManager.TOTAL_FRAMES = cap.size
            return cap
    raise ValueError('Unknown source')


class OpenCVBackend(CameraBackend):

    def __init__(self, name2source: tp.Dict[str, tp.Any], backend: BackendType,  width: int = 1280, height: int = 768, skip_frames: int = 1):
        super().__init__(name2source, width, height)
        self.name2cap: tp.Dict[str, cv2.VideoCapture] = dict()
        default_capture = cv2.VideoCapture if backend == BackendType.OPENCV else lambda url: GCapture(url, CameraManager.FPS)
        for name, url in name2source.items():
            cap = create_capture(url, default_capture)
            self.name2cap[name] = cap
            if not cap.isOpened():
                raise RuntimeError(f"Cannot open capture for {url}")
        camera_manager_logger.info("Succesefully opened all captures")
        self.skip_frames = skip_frames
        self.start_time = time.time() - 10
        self.delay = None
        if CameraManager.FPS is not None and CameraManager.FPS > 0:
            self.delay = 1. / (CameraManager.FPS + 1)

    def _update_view(self):
        for name, cap in self.name2cap.items():
            for _ in range(self.skip_frames):
                has_next, frame = cap.read()
            if not has_next:
                raise RuntimeError(f'VideoCapture {name}|{self.name2source[name]} is empty')
            frame = cv2.resize(frame, (self.width, self.height))

            self.name2view[name] = frame

    def update_view(self):
        if self.delay is None:
            self._update_view()
        else:
            while True:
                t1 = time.time()
                if t1 - self.start_time > self.delay:
                    self._update_view()
                    self.start_time = time.time()
                    break
        
    def set_frame(self, frame_num):
        frame_num = max(min(frame_num, CameraManager.TOTAL_FRAMES-1), 0)
        camera_manager_logger.debug("Setting frame number to %d" % frame_num)
        for name, cap in self.name2cap.items():
            if isinstance(cap, cv2.VideoCapture):
                cap.set(cv2.CAP_PROP_POS_FRAMES, frame_num)
            else:
                cap.set_frame(frame_num)

    def release(self):
        for name, cap in self.name2cap.items():
            cap.release()


class GStreamerBackend(CameraBackend):

    def __init__(self, name2source: tp.Dict[str, tp.Any], backend: BackendType, 
                width: int = 1280, height: int = 768,
                is_h264_encoded: tp.Dict[str, int] = dict()
                ):
        super().__init__(name2source, width, height)
        log_files, urls, is_h264_encoded_list = [], [], []
        for name, url in name2source.items():
            urls.append(url)
            log_files.append('./../logs/%s.log' % name)
            is_h264_encoded_list.append(is_h264_encoded[name])
        
        # Load the library after we are convinced that it is necessary
        from pipeline.stages.input_stage.CGCapture import CGCapture

        self.capture = CGCapture(urls, CameraManager.FPS, width, height, log_files, is_h264_encoded_list, 30)

    def update_view(self):
        frames = self.capture.read()
        self.name2view = dict(zip(self.name2source.keys(), frames))

    def release(self):
        self.capture.release()
