from __future__ import absolute_import

import time

from pipeline.stages.stage import PipelineStage
from pipeline import Task, TaskQueue
from pipeline.meta import DescriptablePool
from utils.monitoring import TimeEstimator
from .CameraManager import CameraManager


class InputStage(PipelineStage):

    def __init__(self, camera_manager, target_queues):
        super().__init__('InputStage', None, target_queues, 'default')
        self.camera_manager = camera_manager
        self.order_id = 0

    def process(self):
        # TODO: fix this by event and create gracefull shutdown
        while True:
            with TimeEstimator(self.object_name):
                self.camera_manager.update_view()

                for cam_name, image in CameraManager.VIEWS.items():
                    task = Task(self.order_id, {'cam_name': cam_name, 'creation_time': time.time()}, {'image': image})
                    
                    for target_queue in self.target_queues:
                        TaskQueue[target_queue].push_data(task.copy())
                    
                self.order_id += 1

    @staticmethod
    def _from_json_dict(json: dict):
        return InputStage(
                    DescriptablePool.from_json_dict(json['manager']),
                    target_queues=json['target_queues']
                    )