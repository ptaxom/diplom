from __future__ import absolute_import

from utils.LoggerManager import get_session_name, LoggerManager
from pipeline.stages.input_stage.CameraManager import USE_GSTREAMER

from datetime import datetime, timedelta
import numpy as np
from ctypes import *
from tqdm import tqdm
import time 
import typing as tp


cgcapture_logger = LoggerManager.get_logger('CGCapture')

def get_encoding(url):
    h_264_patterns = ['264', 'profile2', 'profile2', 'test']

    return any([pattern for pattern in h_264_patterns if pattern in url])


def redirect_gstreamer_logs():
    import os
    from datetime import datetime
    #find all stale logs and append them to global log
    stale_logs = sorted([log_file for log_file in os.listdir('../logs/') if 'gstreamer_session' in log_file])

    os.system('rm ../logs/gstreamer.log')
    for i, stale_log in enumerate(stale_logs[::-1]):
        log_date = datetime.strptime(stale_log.split('gstreamer_session_')[-1].split('.log')[0], '%Y_%m_%d_%H_%M').date()
        if ((datetime.now().date() - log_date) > timedelta(days=4)) or (i > 100):
            os.remove(f'../logs/{stale_log}')
            continue

        os.system(f'echo "\n======== {stale_log} ========" >> ../logs/gstreamer.log')
        os.system(f'cat ../logs/{stale_log} >> ../logs/gstreamer.log')
    
    current_session = '../logs/gstreamer_session_%s.log' % get_session_name()
    os.environ["GST_DEBUG_FILE"] = current_session


LOAD_LIBRARY = USE_GSTREAMER
if LOAD_LIBRARY:
    cgcapture_logger.info('Loading library')
    redirect_gstreamer_logs()
    # Constants area
    lib_path = 'gcapture/build/libpyGCapture.so'
    slave_path = 'gcapture/build/slave'
    MEMKEY = 6000 - 3


    #Wrapper area
    lib = CDLL(lib_path, RTLD_GLOBAL)
    #IPC_COMMAND
    NO_ACTION, SHUTDOWN, RESTART, SLAVE_INITIATED = 0, 1, 2, 3
    #IPC_STATUS
    UNKNOWN, SUCCESS, FAILURE, IN_PROCESS, CLIENT_ERROR = 0, 1, 2, 3, 4

    create_captures = lib.create_captures
    create_captures.argtypes = [POINTER(c_char_p), c_int, c_int, c_int, c_int, c_char_p, c_int, POINTER(c_char_p)] #urls, n_urls, fps, width, height, #log files

    release_captures  = lib.release_captures

    get_pictures_lock_all = lib.get_pictures_lock_all
    get_pictures_lock_all.restype = POINTER(c_int8)
    
    is_running = lib.is_running
    is_running.restype = c_int

    get_event_count = lib.get_event_count
    get_event_count.restype = c_int

    get_status = lib.get_status
    get_status.restype = c_int

    set_command = lib.set_command
    set_command.argtypes = [c_int, c_int] #child_id, IPC_COMMAND
    set_command.restype = c_int

    execute_commands = lib.execute_commands
    execute_commands.restype = c_int

    free = lib.free

def strings_list_to_C_array(strings: tp.List[str]):
    """
        Converts python's list of string to C compatible nested arrays of chars
    """
    c_arr = (c_char_p * len(strings))()
    for i in range(len(strings)):
        c_arr[i] = strings[i].encode('utf-8')
    return c_arr


class CameraContoller:
    # wait 5 seconds between attempts to reload
    RELOAD_TIMEOUT = 5
    
    MAX_ATTEMPTS_PER_OP = 5

    MAX_ATTEMPTS_PER_ERROR = 3
    # If errors occurs frequently than 1 error for 30 minutes exit from algo
    MIN_SUCCESS_TIME = 60 * 30


    def __init__(self, child_id: int, url: str):
        self.child_id = child_id
        self.url = url
        initial_time = time.time() - 3600
        self.last_reloaded = initial_time
        self.n_attempts = 0
        self.last_attempt = initial_time
        self.error_occurs = initial_time - 3600
        
        self.ignore_until = time.time() + 10 # ignore errors first 10 seconds

    def restart_camera(self):
        set_command(self.child_id, RESTART)
        execute_commands()
        self.last_attempt = time.time()

    def update(self):
        camera_status = get_status(self.child_id)
        if camera_status == UNKNOWN:
            return

        if time.time() < self.ignore_until:
            return

        if camera_status == SUCCESS:
            self.n_attempts = 0
            set_command(self.child_id, UNKNOWN)
            
        if camera_status == CLIENT_ERROR: # camera worked normally, but error occurs
            time_without_errors = time.time() - self.error_occurs 
            if time_without_errors < CameraContoller.MIN_SUCCESS_TIME:
                raise RuntimeError(f'Too frequent errors {self.url}')
            self.error_occurs = time.time()
            self.restart_camera()

        if camera_status == FAILURE and time.time() - self.last_attempt > CameraContoller.RELOAD_TIMEOUT: # if failed after reload attempt
            self.n_attempts += 1
            if self.n_attempts >= CameraContoller.MAX_ATTEMPTS_PER_OP:
                spended_time = time.time() - self.error_occurs
                raise RuntimeError(f'Cannot restart {self.url} for {CameraContoller.MAX_ATTEMPTS_PER_OP} attempts in row!! Algorithm failed after {spended_time} seconds.')

            self.restart_camera()
        
class CGCapture:
    
    def __init__(self, urls: tp.List[str], 
                        fps: int, width: int, height:int,
                        log_files: tp.List[str],
                        is_h264_stream: tp.List[int],
                        slave_fps=30):
        self.fps = fps
        self.width = width
        self.height = height
        self.urls = urls
        self.mem_size = width * height * 3
        self.delay = 1. / fps
        self.t0 = time.time()
        total_captures = len(urls)
        self.controllers = [CameraContoller(i, url) for i, url in enumerate(urls)]
        
        urls_arr = strings_list_to_C_array(urls)
        logs_arr = strings_list_to_C_array(log_files)
        encodings_array = (c_int * len(is_h264_stream))(*is_h264_stream)

        error = create_captures(urls_arr, total_captures, 
                                slave_fps, width, height,
                                slave_path.encode('utf-8'), 
                                MEMKEY, logs_arr,
                                encodings_array 
                                )
        if error != 0:
            raise RuntimeError('Cannot open all captures')
        
        
    def grab_frames(self):
        for controller in self.controllers:
            controller.update()

        images = []
        row_data = get_pictures_lock_all()
        
        for im_num in range(len(self.urls)):
            im = np.zeros((self.height, self.width, 3), dtype=np.uint8)
            arr = np.ascontiguousarray(im.flat, dtype=np.uint8)
            data = arr.ctypes.data_as(POINTER(c_byte))
            new_address = cast(addressof(row_data.contents) + self.mem_size * im_num, POINTER(c_char))
            memmove(data, new_address, self.mem_size)
            images.append(im)
        free(row_data)
        return images
            
    def release(self):
        release_captures()
        redirect_gstreamer_logs()

    def read(self):
        while True:
            t1 = time.time()
            if t1 - self.t0 > self.delay:
                images = self.grab_frames()
                self.t0 = time.time()
                break
        return images