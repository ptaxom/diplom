from __future__ import absolute_import


from threading import Thread, Lock
import cv2
import time
from queue import Queue

USE_THREADED = True

class FileVideoStream:
    def __init__(self, path, queue_size=128):
        self.stream = cv2.VideoCapture(path)
        self.size = int(self.stream.get(cv2.CAP_PROP_FRAME_COUNT))
        self.stopped = False

        if USE_THREADED:
            self.lock = Lock()
            self.Q = Queue(maxsize=queue_size)
            self.thread = Thread(target=self.update, args=(), daemon=True)
            self.thread.daemon = True
            self.thread.start()

    def update(self):
        while True:
            if self.stopped:
                break
            
            with self.lock:
                if not self.Q.full():
                    (grabbed, frame) = self.stream.read()

                    if not grabbed:
                        self.stopped = True

                    self.Q.put(frame)
                else:
                    time.sleep(0.1)
        self.stream.release()

    def read(self):
        if USE_THREADED:
            return not self.stopped, self.Q.get()
        else:
            return self.stream.read()

    
    def release(self):
        if USE_THREADED:
            self.stopped = True
            self.thread.join()
        else:
            self.stream.release()

    def isOpened(self):
        return self.stream.isOpened()

    def set_frame(self,  frame_num):
        if USE_THREADED:
            with self.lock:
                self.stream.set(cv2.CAP_PROP_POS_FRAMES, frame_num)
                while not self.Q.empty():
                    self.Q.get()
        else:
            self.stream.set(cv2.CAP_PROP_POS_FRAMES, frame_num)
        