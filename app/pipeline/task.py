from __future__ import absolute_import

import typing as tp
from datetime import date, datetime, timedelta

from copy import deepcopy
from settings import DEBUG

class Task:
    LIFITEME = timedelta(seconds=20)
    
    def __init__(self, order_id, tags: tp.Dict[str, str], data) -> None:
        if DEBUG:
            Task.LIFITEME = timedelta(days=1)

        self.order_id = order_id
        self.tags: tp.Dict[str, str] = tags
        self.invalidation_timeout = datetime.now() + Task.LIFITEME
        self.data = data

    def __str__(self):
        expire_after = (self.invalidation_timeout - datetime.now()).total_seconds()
        return f"""<Task. id: {self.order_id}, tags: {self.tags}, expire after: {expire_after:0.2f}"""

    def __repr__(self) -> str:
        return self.__str__()

    def copy(self):
        return deepcopy(self)