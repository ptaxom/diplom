#pragma once

#include <tkDNN/Yolo3Detection.h>
#include <opencv4/opencv2/opencv.hpp>
#include <vector>
#include <string>
#include <algorithm>

extern "C"
{
  typedef struct Image{
    int w;
    int h;
    uint8_t *data;
} Image;

typedef struct Box{
    float x0, y0, x1, y1;
    float probability;
    int class_id;
} Box;

typedef struct Predictions{
    Box *boxes;
    int n;
} Predictions;

typedef struct BatchPredictions{
    Predictions *predictions;
    int n;
} BatchPredictions;

}


inline float clip(float x)
{
    return std::max(0.f,  std::min(1.f, x));
}

class YoloWrapper : public tk::dnn::Yolo3Detection
{ 
private:
    YoloWrapper() {};
    ~YoloWrapper() {}; 

    static YoloWrapper *instance_;
    
    std::vector<cv::Mat> batch_images;
    int batch_size = 0;
    int n_classes = 0;
    bool inited = false;

    void predict(std::vector<cv::Mat>& frames)
    {
        originalSize.clear();
        for(int bi=0; bi<batch_size;++bi){
            if(!frames[bi].data)
                FatalError("No image data feed to detection");
            originalSize.push_back(frames[bi].size());
            preprocess(frames[bi], bi);    
        }

        tk::dnn::dataDim_t dim = netRT->input_dim;
        dim.n = batch_size;
        netRT->infer(dim, input_d);
        batchDetected.clear();
        for(int bi=0; bi<batch_size;++bi)
            postprocess_custom(bi);
    }

    void postprocess_custom(int bi)
    {
        //get yolo outputs
        std::vector<float *> rt_out;
        //dnnType *rt_out[netRT->pluginFactory->n_yolos];
        for(int i=0; i<netRT->pluginFactory->n_yolos; i++)
            rt_out.push_back((dnnType*)netRT->buffersRT[i+1] + netRT->buffersDIM[i+1].tot()*bi);

        // compute dets
        nDets = 0;
        for(int i=0; i<netRT->pluginFactory->n_yolos; i++) {
            yolo[i]->dstData = rt_out[i];
            yolo[i]->computeDetections(dets, nDets, netRT->input_dim.w, netRT->input_dim.h, confThreshold, yolo[i]->new_coords);
        }
        tk::dnn::Yolo::mergeDetections(dets, nDets, classes, yolo[0]->nms_thresh, yolo[0]->nsm_kind);
        float H = netRT->input_dim.h,
              W = netRT->input_dim.w;
        

        // fill detected
        detected.clear();
        for(int j=0; j<nDets; j++) {
                tk::dnn::Yolo::box b = dets[j].bbox;
                // convert to relative coords
                float x0   = clip((b.x-b.w/2.) / W);
                float x1   = clip((b.x+b.w/2.) / W);
                float y0   = clip((b.y-b.h/2.) / H);
                float y1   = clip((b.y+b.h/2.) / H);

                for(int c=0; c<classes; c++) {
                    if(dets[j].prob[c] >= confThreshold) {
                        int obj_class = c;
                        float prob = dets[j].prob[c];

                        tk::dnn::box res;
                        res.cl = obj_class;
                        res.prob = prob;
                        res.x = x0;
                        res.y = y0;
                        res.w = x1 - x0;
                        res.h = y1 - y0;
                        detected.push_back(res);
                }
            }

        }
        batchDetected.push_back(detected);
    }

public:

    static YoloWrapper* instance()
    {
        if (YoloWrapper::instance_ == nullptr)
        {
            YoloWrapper::instance_ = new YoloWrapper();
            YoloWrapper::instance_->inited = false;
        }
        return YoloWrapper::instance_;
    }

    static void release()
    {
        if (YoloWrapper::instance_ != nullptr)
            delete YoloWrapper::instance_;
        YoloWrapper::instance_ = nullptr;
    }

    static void load_weights(const std::string &tensor_path, const int n_batch, const int network_output_size)
    {
        YoloWrapper::release();
        YoloWrapper* wrapper = YoloWrapper::instance();
        wrapper->init(tensor_path, network_output_size, n_batch);
        wrapper->n_classes = network_output_size;
        wrapper->batch_size = n_batch;
        wrapper->batch_images.clear();
        wrapper->inited = true;
    }

    static void set_threshold(float threshold)
    {
        YoloWrapper::instance()->confThreshold = threshold;
    }

    static void add_to_batch(Image image)
    {
        cv::Mat frame(image.h, image.w, CV_8UC3, image.data);
        YoloWrapper *wrapper = YoloWrapper::instance();
        wrapper->batch_images.push_back(frame);
        if (wrapper->batch_images.size() > wrapper->batch_size)
            wrapper->batch_images.erase(wrapper->batch_images.begin());
    }

    static BatchPredictions predict()
    {
        YoloWrapper *wrapper = YoloWrapper::instance();
        if (!wrapper->inited)
        {
            std::cerr << "Not inited wrapper by tensorRT weights!" << std::endl;
            exit(1);
        }
        if (wrapper->batch_images.size() != wrapper->batch_size)
        {
            std::cerr << "Not enought images to perfom batch detection!" << std::endl;
            exit(1);
        }
        wrapper->predict(wrapper->batch_images);
        BatchPredictions predictions;
        int batch_size = wrapper->batch_size;

        predictions.n = batch_size;
        predictions.predictions = (Predictions*)malloc(batch_size * sizeof(Predictions));
        for(int bi = 0; bi < batch_size; bi++)
        {
            std::vector<tk::dnn::box> &tk_predictions = wrapper->batchDetected[bi];
            predictions.predictions[bi].n = tk_predictions.size();
            predictions.predictions[bi].boxes = (Box*)malloc(tk_predictions.size() * sizeof(Box));
            for(int bbox_id = 0; bbox_id < tk_predictions.size(); bbox_id++)
            {
                Box *box = &predictions.predictions[bi].boxes[bbox_id];
                box->x0 = tk_predictions[bbox_id].x;
                box->y0 = tk_predictions[bbox_id].y;
                box->x1 = tk_predictions[bbox_id].x + tk_predictions[bbox_id].w;
                box->y1 = tk_predictions[bbox_id].y + tk_predictions[bbox_id].h;

                box->probability = tk_predictions[bbox_id].prob;
                box->class_id = tk_predictions[bbox_id].cl;

            }
        }

        return predictions;
    }

    static void reset_buffer()
    {
        YoloWrapper *wrapper = YoloWrapper::instance();
        wrapper->batch_images.clear();
    }

};

extern "C" {

    void init_net(const char *tensor_path, const int batch_size, const int network_output_size);

    void add_image(Image image);

    void set_threshold(float threshold);

    void reset_buffer();

    BatchPredictions predict();

    void free_predictions(BatchPredictions predictions);

}
