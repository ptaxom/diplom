#include <iostream>
#include<iostream>
#include<vector>
#include <tkDNN/tkdnn.h>
#include <tkDNN/DarknetParser.h>


int main(int argc, char  *argv[])
{
    std::string bin_path  = "yolo4_custom";
    if (argc != 4)
    {
        printf("USAGE: [path-to-cfg] [path-to-layers-folder] [path-to .names file]");
        exit(1);
    }
    tk::dnn::Network *net = tk::dnn::darknetParser(argv[1],
     argv[2], 
     argv[3]);
    net->print();
    tk::dnn::NetworkRT *netRT = new tk::dnn::NetworkRT(net, net->getNetworkRTName(bin_path.c_str()));
    return 0;
}