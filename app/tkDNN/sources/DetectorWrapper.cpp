#include <DetectorWrapper.hpp>

YoloWrapper* YoloWrapper::instance_ = nullptr;

extern "C" {

    void init_net(const char *tensor_path, const int batch_size, const int network_output_size)
    {
        YoloWrapper::load_weights(tensor_path, batch_size, network_output_size);
    }

    void add_image(Image image)
    {
        YoloWrapper::add_to_batch(image);
    }

    void set_threshold(float threshold)
    {
        YoloWrapper::set_threshold(threshold);
    }

    void reset_buffer()
    {
        YoloWrapper::reset_buffer();
    }

    BatchPredictions predict()
    {
        return YoloWrapper::predict();
    }

    void free_predictions(BatchPredictions predictions)
    {
        for(int bi = 0; bi < predictions.n; bi++)
            free(predictions.predictions[bi].boxes);
        free(predictions.predictions);
        
    }
}