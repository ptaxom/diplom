from __future__ import absolute_import

from settings import MONITORING, REDIS_DB
from pipeline import Task

import redis
import time
import json
from contextlib import contextmanager

if MONITORING:
    client = redis.Redis('localhost', db=REDIS_DB)
    client.flushall()

def log_queue_status(queue_obj):
    if MONITORING:
        client.sadd('queues', queue_obj.object_name)
        client.set(queue_obj.object_name, len(queue_obj.queue))

def log_task_metrics(key: str, task: Task):
    if MONITORING:
        processed_at = time.time()
        spent_time = processed_at - task.tags['creation_time']
        client.sadd('metrics', key)
        client.lpush(f'{key}_spent_time', spent_time)
        client.lpush(f'{key}_processed', processed_at)

@contextmanager
def TimeEstimator(key_name):
    start_time = time.time()
    try:
        yield
    finally:
        end_time = time.time()
        if MONITORING:
            client.sadd('estimators', key_name)
            client.lpush(key_name, end_time - start_time)
