from __future__ import absolute_import

import threading
import logging
from logging import handlers
from datetime import datetime

from settings import USE_FILE_LOG

MAX_LOG_SIZE = 2 ** 12 # 4MB


def get_session_name():
    now = datetime.now()
    current_session = '%04d_%02d_%02d_%02d_%02d'% (now.year, now.month, now.day, now.hour, now.minute)
    return current_session

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class LoggerManager:
    _INSTANCE = None
    _INIT_LOCK = threading.Lock()
    _GET_LOCK = threading.Lock()
    _LEVEL2COLOR = {
        logging.NOTSET: '',
        logging.DEBUG: bcolors.OKBLUE,
        logging.INFO: bcolors.OKGREEN,
        logging.WARNING: bcolors.WARNING,
        logging.ERROR: bcolors.FAIL,
        logging.critical: bcolors.FAIL + bcolors.BOLD
    }

    def __init__(self):
        with LoggerManager._INIT_LOCK:
            if LoggerManager._INSTANCE is None:
                old_record_factory = logging.getLogRecordFactory()
                
                def record_factory(*args, **kwargs):
                    # add custom color property
                    record = old_record_factory(*args, **kwargs)
                    record.color = LoggerManager._LEVEL2COLOR.get(record.levelno, '')
                    return record
                
                logging.setLogRecordFactory(record_factory)

                self.inited_logger_names = []
                self.handlers = []
                
                stream_handler = logging.StreamHandler()
                stream_handler.setLevel(logging.DEBUG)
                # stream_handler.setLevel(logging.INFO if GlobalConfig['inference'] else logging.DEBUG)
                stream_handler.setFormatter(logging.Formatter('%(color)s[%(asctime)s %(levelname)s %(name)s]: %(message)s' + bcolors.ENDC))
                self.handlers.append(stream_handler)
                
                formatter = logging.Formatter('[%(asctime)s %(levelname)s %(name)s]: %(message)s')
                if USE_FILE_LOG:
                    file_handler = handlers.RotatingFileHandler('../logs/MCVC.log', mode='a+', backupCount=1, maxBytes=MAX_LOG_SIZE) 
                    file_handler.setFormatter(formatter)
                    file_handler.setLevel(logging.WARNING)

                    session_name = '../logs/MCVC_%s.log' % get_session_name()
                    session_handler = handlers.RotatingFileHandler(session_name, mode='a+', backupCount=1, maxBytes=MAX_LOG_SIZE)
                    session_handler.setFormatter(formatter)
                    session_handler.setLevel(logging.WARNING)
            
                    self.handlers += [session_handler, file_handler]

                LoggerManager._INSTANCE = self

    @staticmethod
    def get_logger(logger_name):
        with LoggerManager._GET_LOCK:
            LoggerManager()
            self = LoggerManager._INSTANCE
            if logger_name not in self.inited_logger_names:
                self.inited_logger_names.append(logger_name)

                logger = logging.getLogger(logger_name)
                logger.setLevel(logging.INFO)
                for handler in self.handlers:
                    logger.addHandler(handler)

            return logging.getLogger(logger_name)
