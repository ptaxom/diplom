from __future__ import absolute_import

import redis
import os, sys
import time
import argparse, subprocess

GPU_UTIL = []
GPU_MEM =  []
GPU_WINDOW_SIZE = 10

def check_gpu():
    data = subprocess.check_output('nvidia-smi --query-gpu=utilization.gpu,utilization.memory --format=csv | tail -n 1', shell=True).decode('utf-8')
    util, mem = list(map(lambda x: float(x.strip().split(' ')[0]), data.split(',')))
    measure_time = time.time()
    GPU_UTIL.append((measure_time, util))
    GPU_MEM.append(mem)

    while GPU_UTIL:
        m_time, _ = GPU_UTIL[0]
        if measure_time - m_time > GPU_WINDOW_SIZE:
            GPU_UTIL.pop(0)
            GPU_MEM.pop(0)
        else:
            break



sys.path.append('../')
from settings import REDIS_DB

parser = argparse.ArgumentParser(description='Detect transaction at park')
parser.add_argument('--host', dest='host', action='store',
                    required=False, default='localhost',
                    type=str,
                    help='Redis host')
parser.add_argument('-d', '--db', dest='db', action='store',
                    required=False, default=REDIS_DB,
                    type=int,
                    help='Redis db')
args = parser.parse_args()

def load_float_list(client, key):
    total = client.llen(key)
    flist = [float(x.decode('utf-8')) for x in client.lrange(key, 0, total)]
    return flist

def average(l):
    l = list(l)
    return sum(l) / len(l) if l else 0.

def compute_throughput(timestamps, window_size):
    if not timestamps:
        return 0
    last = max(timestamps)
    return sum([x > last - window_size for x in timestamps])

def prepare_string(client):
    lines = []
    keys = client.smembers('queues')
    keys = sorted([x.decode('utf-8') for x in keys])
    if not keys:
        GPU_MEM.clear(); GPU_UTIL.clear()
    check_gpu()
    lines += [
        f'GPU utilization: {average(map(lambda x: x[1], GPU_UTIL)):0.2f}%',
        f'GPU memory:      {average(GPU_MEM):0.2f}%',
        ''
    ]
    for key in keys:
        val = client.get(key)
        n_tasks = -1
        if val is not None:
            n_tasks = int(val.decode('utf-8'))
        lines.append(f'{key:15s}: {n_tasks:05d}')

    for metric_key in client.smembers('metrics'):
        key = metric_key.decode('utf-8')
        spent_time = load_float_list(client, f'{key}_spent_time')
        avg = average(spent_time)
        throughtput = load_float_list(client, f'{key}_processed')
        WS = [1, 5, 10]
        throughtputs = '\\'.join(map(lambda x: f'{x:0.1f}', [compute_throughput(throughtput, ws) for ws in WS]))
        desc = '\\'.join(map(lambda x: f'{x:0.0f}s', WS))
        lines += [
            f'Metrics from {key}: ',
            f'  Processed {len(spent_time)} with avg {avg:0.2f}s. per task',
            f'  Throughput: {throughtputs} ({desc})',
            ''
        ]
    lines.append('Execution of stages:')
    for stage in sorted(list(client.smembers('estimators'))):
        stage = stage.decode('utf-8')
        exec_time = average(load_float_list(client, stage))
        lines.append(f'  {stage:15s}: {exec_time:0.2f}ms.')

    return f'Monitoring for {len(keys)} queues\n\n' + '\n'.join(lines)

if __name__ == '__main__':
    client = redis.Redis(args.host, db=args.db)
    prev_status = ''
    while True:
        current_status = prepare_string(client)
        if prev_status != current_status:
            prev_status = current_status
            os.system('clear')
            print(current_status)
        time.sleep(0.1)
