import cv2
import numpy as np
import base64

PREVIEW_WIDTH, PREVIEW_HEIGHT = 640, 360

W, H = 600, 400

def scale_to_tile(frame, tile_size):
    """
        Scale single frame to tile size and if neccesary convert to BGR
    """
    f = cv2.resize(frame, tile_size, interpolation=cv2.INTER_AREA)
    if len(f.shape) == 2 or f.shape[2] == 1:  # Gray picture
        f = cv2.cvtColor(f, cv2.COLOR_GRAY2BGR)
    return f


def show(wnd_name, frame, tile_size):
    """
        Showing single frame
    """
    cv2.imshow(wnd_name, scale_to_tile(frame, tile_size))


def renderNrows(wnd_name, n_rows, tiles, tile_names, tile_size, display=True):
    """
        Render batch of frame in n_rows tile grid with names and sizes
    """
    img_shape = (tile_size[1], tile_size[0], 3)
    scaled = [np.zeros(img_shape) if x is None else scale_to_tile(
        x, tile_size) for x in tiles]

    for i, name in enumerate(tile_names):
        if not name is None and i < len(tiles):
            width = len(name) * 14
            cv2.putText(scaled[i], name,
                        ((tile_size[0] - width) // 2, 40),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    m = (len(tiles) + n_rows - 1) // n_rows
    for i in range(m * n_rows - len(scaled)):
        scaled.append(np.zeros(img_shape))

    rows = []
    for i in range(n_rows):
        rows.append(np.hstack(scaled[i * m: i * m + m]))

    img = np.uint8(np.vstack(rows))
    if display:
        cv2.imshow(wnd_name, img)
    return img


def plot_bboxes_id_color(frame, bboxes, default_color=(0, 0, 255)):
    H, W = frame.shape[:2]

    n_dets = bboxes.shape[0]
    ids = np.tile([-1], (n_dets, 1))
    if bboxes.shape[1] == 5:
        ids = bboxes[:, -1]
    ids = ids.reshape(-1, 1)

    if bboxes.shape[1] < 8:
        bboxes = np.hstack([bboxes[:, :4],
                            ids,
                            np.tile(default_color, (n_dets, 1)),
                        ])
    for bbox in bboxes:
        color = tuple([int(x) for x in bbox[5:8]])
        idx = bbox[4]
        bbox = bbox[:4]
        bb_old = bbox
        bbox = np.multiply(bbox, np.array([W, H, W, H])).astype('int')
        p1 = tuple(bbox[:2])
        p2 = tuple(bbox[2:])
        p3 = tuple([(p1[0] + p2[0]) // 2, (p1[1] + p2[1]) // 2])
        try:
            cv2.rectangle(frame, p1, p2, color, 2)
            if idx >= 0:
                cv2.putText(frame, "# %d" % idx, p3,
                            cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 1)
        except Exception as e:
            print(e)
            print(p1, p2, p3, bb_old)
            exit(1)


def plot_bboxes_id(frame, bboxes):
    H, W = frame.shape[:2]
    if bboxes.shape[1] == 4:
        bboxes = np.hstack([bboxes, np.zeros((bboxes.shape[0], 1)) - 1])
    for bbox in bboxes:
        idx = bbox[-1]
        bbox = bbox[:-1]
        bbox = np.multiply(bbox, np.array([W, H, W, H])).astype('int')
        p1 = tuple(bbox[:2])
        p2 = tuple(bbox[2:])
        cv2.rectangle(frame, p1, p2, (255, 0, 0), 2)
        p3 = tuple([(p1[0] + p2[0]) // 2, (p1[1] + p2[1]) // 2])
        if idx >= 0:
            cv2.putText(frame, "# %d" % idx, p3,
                        cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 1)

def compress_image(image: np.ndarray, width: int, height: int) -> str:
    preview = cv2.resize(image, (width, height))
    _, buffer = cv2.imencode('.jpg', preview)
    preview = base64.b64encode(buffer).decode('utf-8')
    return preview
    